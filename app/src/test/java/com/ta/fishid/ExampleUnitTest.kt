package com.ta.fishid

import com.ta.fishid.util.Utility
import org.junit.Test

import org.junit.Assert.*
import java.util.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }

    @Test
    fun testArray() {
        val mutableList: MutableList<String> = arrayListOf()
        if (mutableList.isNullOrEmpty())
            mutableList.add(0, "0")
        mutableList.add(1, "1")
        mutableList.add(2, "2")
        mutableList.add(3, "3")
        mutableList.removeAt(1)

        print(mutableList)
    }

    @Test
    fun testCalendar() {
        val cal = Calendar.getInstance()

        cal.set(Calendar.HOUR, Date().hours)
        println(cal.get(Calendar.HOUR_OF_DAY))
        println(cal.timeInMillis)

        cal.set(Calendar.HOUR_OF_DAY, Date().hours)
        println(cal.get(Calendar.HOUR_OF_DAY))
        println(cal.timeInMillis)
    }

    @Test
    fun testRegex() {
        val text = "Agusle onard-o1"
        if (text.matches(Regex("^[a-zA-Z0-9._-]{3,}\$")))
            println("YES")
    }
}
