package com.ta.fishid

object Constants {
    const val BaseURL = "http://192.168.100.30:5000/"
//    const val BaseURL = "http://192.168.43.19:5000/"
//    const val BaseURL = "http://10.0.2.2:5000/"

    const val GALLERY_IMAGE = 18
    const val REQUEST_TAKE_PHOTO = 97

    const val SUCCESS = 0
    const val LOADING = 1
    const val FAILURE = 2

    const val ChannelSupplier = "Supplier"

    const val Type = "Type"
    const val Like = "Like"
    const val History = "History"
    const val SubHistory = "Sub History"

    val listHari = listOf(
        "Minggu",
        "Senin",
        "Selasa",
        "Rabu",
        "Kamis",
        "Jumat",
        "Sabtu"
    )
    val listBulan = listOf(
        "Januari",
        "Februari",
        "Maret",
        "April",
        "Mei",
        "Juni",
        "Juli",
        "Agustus",
        "September",
        "Oktober",
        "November",
        "Desember"
    )

    val arrayBentukIkan =
        arrayOf(
            "Not selected",
            "Angelfish",
            "Barracuda",
            "Batfish",
            "Butterflyfish",
            "Damselfish",
            "Fusilier",
            "Grouper",
            "Parrotfish",
            "Pufferfish",
            "Snapperfish",
            "Soldierfish",
            "Surgeonfish",
            "Sweetlips",
            "Triggerfish",
            "Wrasse"
        )
    val arrayImageBentukIkan = arrayOf(
        null,
        R.drawable.fish_shapes_angelfish, R.drawable.fish_shapes_barracuda,
        R.drawable.fish_shapes_batfish, R.drawable.fish_shapes_butterflyfish,
        R.drawable.fish_shapes_damselfish, R.drawable.fish_shapes_fusilier,
        R.drawable.fish_shapes_grouper, R.drawable.fish_shapes_parrotfish,
        R.drawable.fish_shapes_pufferfish, R.drawable.fish_shapes_snapperfish,
        R.drawable.fish_shapes_soldierfish, R.drawable.fish_shapes_surgeonfish,
        R.drawable.fish_shapes_sweetlips, R.drawable.fish_shapes_triggerfish,
        R.drawable.fish_shapes_wrasse
    )
    val arrayMulutIkan =
        arrayOf(
            "Not selected",
            "Superior",
            "Terminal",
            "Subterminal",
            "Inferior",
            "Barbels",
            "Tubular",
            "Elongated"
        )
    val arrayImageMulutIkan = arrayOf(
        null,
        R.drawable.fish_mouths_superior, R.drawable.fish_mouths_terminal,
        R.drawable.fish_mouths_subterminal, R.drawable.fish_mouths_inferior,
        R.drawable.fish_mouths_barbels, R.drawable.fish_mouths_tubular,
        R.drawable.fish_mouths_elongated
    )
    val arrayEkorIkan =
        arrayOf(
            "Not selected",
            "Forked",
            "Truncate",
            "Rounded",
            "Lunate",
            "Lanceolate",
            "Eel-like"
        )
    val arrayImageEkorIkan = arrayOf(
        null,
        R.drawable.fish_tails_forked, R.drawable.fish_tails_truncate,
        R.drawable.fish_tails_rounded, R.drawable.fish_tails_lunate,
        R.drawable.fish_tails_lanceolate, R.drawable.fish_tails_eel
    )
    val arraySiripIkan =
        arrayOf(
            "Not selected",
            "Notched (spiny)",
            "Continuous",
            "Separate dorsal fins"
        )
    val arrayImageSiripIkan = arrayOf(
        null,
        R.drawable.fish_fins_notched, R.drawable.fish_fins_continuous,
        R.drawable.fish_fins_separate
    )
    val arrayCorakIkan =
        arrayOf(
            "Not selected",
            "Vertical",
            "Horizontal",
            "Diagonal",
            "Chevron",
            "Spotted or false eye",
            "Saddled",
            "Banded",
            "Spotted",
            "Blotched",
            "Painted"
        )
    val arrayImageCorakIkan = arrayOf(
        null,
        R.drawable.fish_markings_vertical, R.drawable.fish_markings_horizontal,
        R.drawable.fish_markings_diagonal, R.drawable.fish_markings_chevron,
        R.drawable.fish_markings_spotted_false, R.drawable.fish_markings_saddled,
        R.drawable.fish_markings_banded, R.drawable.fish_markings_spotted,
        R.drawable.fish_markings_blotched, R.drawable.fish_markings_painted
    )
}