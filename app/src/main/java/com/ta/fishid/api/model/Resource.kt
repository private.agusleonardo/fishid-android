package com.ta.fishid.api.model

data class Resource<out T>(val status: Status, val data: T?, val message: String?) {
    companion object {
        fun <T> success(data: T?): Resource<T> {
            return Resource(Status.SUCCESS, data, null)
        }

        fun <T> error(msg: String, data: T?): Resource<T> {
            return Resource(Status.ERROR, data, msg)
        }

        fun <T> empty(): Resource<T> {
            return Resource(Status.EMPTY, null, null)
        }

        fun <T> accepted(): Resource<T> {
            return Resource(Status.ACCEPTED, null, null)
        }

        fun <T> unauthorized(msg: String, data: T?): Resource<T> {
            return Resource(Status.UNAUTH, data, msg)
        }

        fun <T> loading(data: T?): Resource<T> {
            return Resource(Status.LOADING, data, null)
        }

        fun <T> validationError(msg: String, data: T?): Resource<T> {
            return Resource(Status.VALIDATION_ERROR, data, msg)
        }

        fun <T> gone(msg: String, data: T?): Resource<T> {
            return Resource(Status.GONE, data, msg)
        }
    }
}