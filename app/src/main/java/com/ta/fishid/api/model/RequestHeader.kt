package com.ta.fishid.api.model

data class RequestHeader(
    var accesstoken: AccessToken,
    val language: String
)