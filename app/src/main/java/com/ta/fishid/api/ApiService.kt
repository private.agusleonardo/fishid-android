package com.ta.fishid.api

import androidx.lifecycle.LiveData
import com.ta.fishid.api.model.BaseDataResponse
import com.ta.fishid.api.model.BaseListResponse
import com.ta.fishid.api.model.BaseResponse
import com.ta.fishid.model.*
import io.reactivex.Observable
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*

interface ApiService {
    @FormUrlEncoded
    @POST("api/register")
    fun register(@FieldMap params: Map<String?, String?>?): LiveData<ApiResponse<BaseDataResponse<UserData>>>

    @FormUrlEncoded
    @POST("api/login")
    fun login(@FieldMap params: Map<String?, String?>?): LiveData<ApiResponse<BaseDataResponse<UserData>>>

    @FormUrlEncoded
    @POST("api/token/simpan")
    fun postFirebaseToken(@FieldMap params: Map<String?, String?>?): Observable<BaseResponse>

    @GET("api/profile/{userId}")
    fun getProfile(
        @Path("userId") userId: String,
        @Query("page") page: String,
        @Query("perPage") perPage: String,
        @Query("offset") offset: String
    ): LiveData<ApiResponse<BaseDataResponse<ProfileData>>>

    @Multipart
    @POST("api/profile/{userId}")
    fun postProfile(
        @Path("userId") userId: String,
        @PartMap params: Map<String?, @JvmSuppressWildcards RequestBody?>?
    ): LiveData<ApiResponse<BaseDataResponse<UserData>>>

    @Multipart
    @POST("api/profile/{userId}")
    fun postProfile(
        @Path("userId") userId: String,
        @Part picture: MultipartBody.Part,
        @PartMap params: Map<String?, @JvmSuppressWildcards RequestBody?>?
    ): LiveData<ApiResponse<BaseDataResponse<UserData>>>

    @FormUrlEncoded
    @POST("api/password")
    fun postPassword(
        @FieldMap params: Map<String?, String?>?
    ): LiveData<ApiResponse<BaseResponse>>

    @Multipart
    @POST("api/post")
    fun postPost(
        @Part picture: MultipartBody.Part,
        @Part fishId: List<MultipartBody.Part>?,
        @Part x1: List<MultipartBody.Part>?,
        @Part y1: List<MultipartBody.Part>?,
        @Part x2: List<MultipartBody.Part>?,
        @Part y2: List<MultipartBody.Part>?,
        @Part probability: List<MultipartBody.Part>?,
        @Part status: List<MultipartBody.Part>?,
        @PartMap params: Map<String?, @JvmSuppressWildcards RequestBody?>?
    ): LiveData<ApiResponse<BaseResponse>>

    @Multipart
    @POST("api/detection/{user_id}")
    fun postDetect(
        @Path("user_id") userId: String,
        @Part picture: MultipartBody.Part
    ): LiveData<ApiResponse<BaseListResponse<DetectData>>>

    @GET("api/filter/fish")
    fun getFish(): LiveData<ApiResponse<BaseListResponse<FilterData>>>

    @GET("api/explore/{fish_id}")
    fun getExplore(@Path("fish_id") userId: String, @Query("page") page: String, @Query("perPage") perPage: String): LiveData<ApiResponse<BaseDataResponse<ExploreData>>>

    @GET("api/home/{user_id}")
    fun getHome(
        @Path("user_id") userId: String, @Query("page") page: String, @Query("perPage") perPage: String,
        @Query("offset") offset: String
    ): LiveData<ApiResponse<BaseListResponse<PostData>>>

    @GET("api/post/{user_id}/{post_id}")
    fun getPost(@Path("user_id") userId: String, @Path("post_id") postId: String): LiveData<ApiResponse<BaseDataResponse<PostData>>>

    @FormUrlEncoded
    @POST("api/report")
    fun report(
        @FieldMap params: Map<String?, String?>?
    ): LiveData<ApiResponse<BaseResponse>>

    @FormUrlEncoded
    @POST("api/post/delete")
    fun delete(
        @FieldMap params: Map<String?, String?>?
    ): LiveData<ApiResponse<BaseResponse>>

    @FormUrlEncoded
    @POST("api/like")
    fun like(
        @FieldMap params: Map<String?, String?>?
    ): LiveData<ApiResponse<BaseResponse>>

    @FormUrlEncoded
    @POST("api/unlike")
    fun unlike(
        @FieldMap params: Map<String?, String?>?
    ): LiveData<ApiResponse<BaseResponse>>

    @GET("api/comment/{post_id}")
    fun getComment(@Path("post_id") userId: String): LiveData<ApiResponse<BaseDataResponse<CommentData>>>

    @FormUrlEncoded
    @POST("api/comment")
    fun postComment(
        @FieldMap params: Map<String?, String?>?
    ): LiveData<ApiResponse<BaseResponse>>

    @GET("api/collection/like/{userId}")
    fun getCollectionLike(
        @Path("userId") userId: String
    ): LiveData<ApiResponse<BaseDataResponse<ProfileData>>>

    @GET("api/collection/history/{userId}")
    fun getCollectionHistory(
        @Path("userId") userId: String
    ): LiveData<ApiResponse<BaseListResponse<HistoryData>>>

    @FormUrlEncoded
    @PUT("api/comment/disable")
    fun disableComment(@FieldMap params: Map<String?, String?>?): LiveData<ApiResponse<BaseResponse>>
}