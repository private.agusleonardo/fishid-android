package com.ta.fishid.api.model

enum class Status {
    SUCCESS,
    ERROR,
    EMPTY,
    ACCEPTED,
    UNAUTH,
    LOADING,
    VALIDATION_ERROR,
    GONE
}