package com.ta.fishid.api

import android.util.Log
import com.ta.fishid.api.model.RequestHeader
import okhttp3.Interceptor
import okhttp3.Response

class RequestInterceptor(private val requestHeaders: RequestHeader) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val original = chain.request()
        val builder =
            original.newBuilder()
                .header("jwt_token", "Bearer " + requestHeaders.accesstoken.accessToken)
                .header("Accept", requestHeaders.language)
                .header("User-Agent", "okhttp")
                .method(original.method, original.body)
        val newRequest = builder.build()
        Log.e("token", requestHeaders.accesstoken.accessToken)

        return chain.proceed(newRequest)
    }
}