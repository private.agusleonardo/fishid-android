package com.ta.fishid.api

import android.util.Log
import retrofit2.Response
import java.util.regex.Pattern

@Suppress("unused") // T is used in extending classes
sealed class ApiResponse<T> {
    companion object {
        fun <T> create(error: Throwable): ApiErrorResponse<T> {
            return ApiErrorResponse(error.message ?: "unknown error")
        }

        fun <T> create(response: Response<T>): ApiResponse<T> {
            return when {
                response.isSuccessful -> {
                    val body = response.body()
                    when {
                        body == null || response.code() == 204 -> ApiEmptyResponse()
                        response.code() == 202 -> ApiAcceptedResponse()
                        else -> ApiSuccessResponse(
                            body = body,
                            linkHeader = response.headers().get("jwt_token")
                        )
                    }
                }
                else -> {
                    val msg = response.errorBody()?.string()
                    when (response.code()) {
                        401 -> {
                            val errorMsg = when {
                                msg.isNullOrEmpty() -> response.message()
                                else -> msg
                            }
                            ApiUnauthorizedResponse(errorMsg ?: "unknown error")
                        }
                        410 -> {
                            val errorMsg = when {
                                msg.isNullOrEmpty() -> response.message()
                                else -> msg
                            }
                            ApiGoneResponse(errorMsg ?: "unknown error")
                        }
                        422 -> {
                            val errorMsg = when {
                                msg.isNullOrEmpty() -> response.message()
                                else -> msg
                            }
                            ApiValidationErrorResponse(errorMsg ?: "unknown error")
                        }
                        else -> {
                            val errorMsg = when {
                                msg.isNullOrEmpty() -> response.message()
                                else -> msg
                            }
                            ApiErrorResponse(errorMsg ?: "unknown error")
                        }
                    }
                }
            }
        }
    }
}

/**
 * separate class for HTTP 204 resposes so that we can make ApiSuccessResponse's body non-null.
 */
class ApiAcceptedResponse<T> : ApiResponse<T>()

class ApiEmptyResponse<T> : ApiResponse<T>()
class ApiGoneResponse<T>(val errorMessage: String) : ApiResponse<T>()
class ApiUnauthorizedResponse<T>(val errorMessage: String) : ApiResponse<T>()
class ApiValidationErrorResponse<T>(val errorMessage: String) : ApiResponse<T>()

data class ApiSuccessResponse<T>(
    val body: T,
    val links: Map<String, String>
) : ApiResponse<T>() {
    constructor(body: T, linkHeader: String?) : this(
        body = body,
        links = linkHeader?.extractLinks() ?: emptyMap()
    )

    val nextPage: Int? by lazy(LazyThreadSafetyMode.NONE) {
        links[NEXT_LINK]?.let { next ->
            val matcher = PAGE_PATTERN.matcher(next)
            if (!matcher.find() || matcher.groupCount() != 1) {
                null
            } else {
                try {
                    Integer.parseInt(matcher.group(1))
                } catch (ex: NumberFormatException) {
                    Log.d("cannot parse next page ", next)
                    null
                }
            }
        }
    }

    companion object {
        private val LINK_PATTERN = Pattern.compile("<([^>]*)>[\\s]*;[\\s]*rel=\"([a-zA-Z0-9]+)\"")
        private val PAGE_PATTERN = Pattern.compile("\\bpage=(\\d+)")
        private const val NEXT_LINK = "next"

        private fun String.extractLinks(): Map<String, String> {
            val links = mutableMapOf<String, String>()
            links["jwt_token"] = this
            return links
        }

    }
}

data class ApiErrorResponse<T>(val errorMessage: String) : ApiResponse<T>()