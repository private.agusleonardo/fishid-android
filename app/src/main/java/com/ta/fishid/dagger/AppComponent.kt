package com.ta.fishid.dagger

import com.ta.fishid.FishIdApp
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        AppModule::class
    ]
)
interface AppComponent : AndroidInjector<FishIdApp> {
    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<FishIdApp>()
}