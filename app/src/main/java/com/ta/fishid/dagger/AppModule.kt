package com.ta.fishid.dagger

import android.content.Context
import com.ta.fishid.Constants
import com.ta.fishid.FishIdApp
import com.ta.fishid.api.ApiService
import com.ta.fishid.api.RequestInterceptor
import com.ta.fishid.api.model.AccessToken
import com.ta.fishid.api.model.RequestHeader
import com.ta.fishid.model.LocalUserData
import com.ta.fishid.util.LiveDataCallAdapterFactory
import dagger.Module
import dagger.Provides
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module(
    includes = [ViewModelModule::class,
        ActivityBuildersModule::class,
        FragmentBuilderModule::class]
)
class AppModule {
    @Singleton
    @Provides
    fun provideContext(application: FishIdApp): Context {
        return application.applicationContext
    }

    @Singleton
    @Provides
    fun provideLocalUserData(): LocalUserData {
        return LocalUserData()
    }

    @Singleton
    @Provides
    fun provideHttpLogingInterceptor(): HttpLoggingInterceptor {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        return logging
    }

    @Singleton
    @Provides
    fun provideRequestHeader(): RequestHeader {
        return RequestHeader(AccessToken(), "application/json")
    }

    @Singleton
    @Provides
    fun provideRequestInterceptor(requestHeader: RequestHeader): RequestInterceptor {
        return RequestInterceptor(requestHeader)
    }

    @Singleton
    @Provides
    fun provideOkHttpClient(
        logging: HttpLoggingInterceptor,
        requestInterceptor: RequestInterceptor
    ): OkHttpClient {
//        var cipherSuites: MutableList<CipherSuite>? = ConnectionSpec.MODERN_TLS.cipherSuites()
//        if (!cipherSuites!!.contains(CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA)) {
//            cipherSuites = ArrayList(cipherSuites)
//            cipherSuites.add(CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA)
//        }
//        val spec = ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
//            .cipherSuites(*cipherSuites.toTypedArray())
//            .build()
        return OkHttpClient.Builder()
//            .connectionSpecs(Collections.singletonList(spec))
            .connectTimeout(20, TimeUnit.SECONDS)
            .readTimeout(20, TimeUnit.SECONDS)
            .writeTimeout(20, TimeUnit.SECONDS)
            .addInterceptor(logging)
            .addInterceptor(requestInterceptor)
            .build()

    }

    @Singleton
    @Provides
    fun provideGithubService(client: OkHttpClient): ApiService {
        return Retrofit.Builder()
            .baseUrl(Constants.BaseURL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(LiveDataCallAdapterFactory())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .build()
            .create(ApiService::class.java)
    }

}