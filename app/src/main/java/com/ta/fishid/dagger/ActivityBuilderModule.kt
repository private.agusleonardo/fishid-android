package com.ta.fishid.dagger

import com.ta.fishid.firebase.AppFirebaseMessagingService
import com.ta.fishid.ui.comment.CommentActivity
import com.ta.fishid.ui.login.LoginActivity
import com.ta.fishid.ui.login.RegisterActivity
import com.ta.fishid.ui.main.MainActivity
import com.ta.fishid.ui.post.PostActivity
import com.ta.fishid.ui.postDetail.PostDetailActivity
import com.ta.fishid.ui.profile.ChangePasswordActivity
import com.ta.fishid.ui.profile.EditProfileActivity
import com.ta.fishid.ui.profile.ProfileActivity
import com.ta.fishid.ui.splashscreen.SplashScreenActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
internal abstract class ActivityBuildersModule {
    @ContributesAndroidInjector
    abstract fun bindAppFirebaseMessagingService(): AppFirebaseMessagingService

    @ContributesAndroidInjector
    internal abstract fun bindSplashScreenActivity(): SplashScreenActivity

    @ContributesAndroidInjector
    internal abstract fun bindMainActivity(): MainActivity

    @ContributesAndroidInjector
    internal abstract fun bindLoginActivity(): LoginActivity

    @ContributesAndroidInjector
    internal abstract fun bindRegisterActivity(): RegisterActivity

    @ContributesAndroidInjector
    internal abstract fun bindEditProfileActivity(): EditProfileActivity

    @ContributesAndroidInjector
    internal abstract fun bindChangePasswordActivity(): ChangePasswordActivity

    @ContributesAndroidInjector
    internal abstract fun bindCommentActivity(): CommentActivity

    @ContributesAndroidInjector
    internal abstract fun bindPostActivity(): PostActivity

    @ContributesAndroidInjector
    internal abstract fun bindPostDetailActivity(): PostDetailActivity

    @ContributesAndroidInjector
    internal abstract fun bindProfileActivity(): ProfileActivity
}