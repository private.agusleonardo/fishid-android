package com.ta.fishid.dagger

import com.ta.fishid.ui.collection.CollectionFragment
import com.ta.fishid.ui.explore.ExploreFragment
import com.ta.fishid.ui.home.HomeFragment
import com.ta.fishid.ui.profile.ProfileFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class FragmentBuilderModule {

    @ContributesAndroidInjector
    abstract fun bindHomeFragment(): HomeFragment

    @ContributesAndroidInjector
    abstract fun bindExploreFragment(): ExploreFragment

    @ContributesAndroidInjector
    abstract fun bindProfileFragment(): ProfileFragment

    @ContributesAndroidInjector
    abstract fun bindCollectionFragment(): CollectionFragment
}