package com.ta.fishid.ui.profile

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.ta.fishid.R
import com.ta.fishid.api.model.Status
import com.ta.fishid.databinding.ActivityChangePasswordBinding
import com.ta.fishid.ui.base.BaseActivity
import org.json.JSONObject
import javax.inject.Inject

class ChangePasswordActivity : BaseActivity() {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var viewModel: ProfileViewModel
    private lateinit var binding: ActivityChangePasswordBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_change_password)
        viewModel = initViewModel(ProfileViewModel::class.java, viewModelFactory)
        binding.viewModel = viewModel

        setToolbar("Change Password", true)
        initObserver()
    }

    private fun initObserver() {
        viewModel.checkPassword.observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    Toast.makeText(this, "Password changed successfully", Toast.LENGTH_SHORT).show()
                    finish()
                }
                Status.ERROR -> {
                    Toast.makeText(this, "Failed change password", Toast.LENGTH_SHORT).show()
                }
                Status.VALIDATION_ERROR -> {
                    val json = JSONObject(it.message!!)
                    Toast.makeText(this, json.getString("message"), Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_save, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val oldPassword = binding.etOldPassword.text.toString()
        val newPassword = binding.etNewPassword.text.toString()
        val confirmPassword = binding.etConfirmPassword.text.toString()

        if (oldPassword.isNotEmpty() && newPassword.isNotEmpty() && confirmPassword.isNotEmpty())
            if (newPassword.count() < 8 || newPassword.count() > 16)
                Toast.makeText(
                    this,
                    "Your password must be between 8 and 16 characters",
                    Toast.LENGTH_SHORT
                ).show()
            else if (confirmPassword == newPassword) {
                viewModel.oldPassword.set(oldPassword)
                viewModel.newPassword.set(newPassword)
                viewModel.postPassword()
            } else Toast.makeText(
                this,
                "Password and confirm password must be the same",
                Toast.LENGTH_SHORT
            ).show()
        else Toast.makeText(
            this,
            "Form must not empty",
            Toast.LENGTH_SHORT
        ).show()
        return super.onOptionsItemSelected(item)
    }

}