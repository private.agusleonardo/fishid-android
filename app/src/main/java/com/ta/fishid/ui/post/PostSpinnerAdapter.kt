package com.ta.fishid.ui.post

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import com.ta.fishid.R


class SpinnerAdapter(
    context: Context, resource: Int,
    val textArray: Array<String>,
    val imageArray: Array<Int?>
) : ArrayAdapter<String?>(
    context,
    R.layout.spinner_fish_category,
    R.id.spinnerTextView,
    textArray
) {

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup?): View {
        return getCustomView(position, convertView, parent)
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        return getCustomView(position, convertView, parent)
    }

    private fun getCustomView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val inflater =
            context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val row: View = inflater.inflate(R.layout.spinner_fish_category, parent, false)
        val textView = row.findViewById(R.id.spinnerTextView) as TextView
        textView.text = textArray[position]
        val imageView: ImageView = row.findViewById(R.id.spinnerImages) as ImageView
        if (imageArray[position] != null)
            imageView.setImageResource(imageArray[position]!!)
        return row
    }

}