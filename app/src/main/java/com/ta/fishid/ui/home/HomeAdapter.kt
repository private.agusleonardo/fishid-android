package com.ta.fishid.ui.home

import android.graphics.*
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.text.SpannableStringBuilder
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.graphics.drawable.toDrawable
import androidx.core.text.bold
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.target.Target
import com.bumptech.glide.request.transition.Transition
import com.ta.fishid.Constants
import com.ta.fishid.Constants.arrayBentukIkan
import com.ta.fishid.Constants.arrayCorakIkan
import com.ta.fishid.Constants.arrayEkorIkan
import com.ta.fishid.Constants.arrayImageBentukIkan
import com.ta.fishid.Constants.arrayImageCorakIkan
import com.ta.fishid.Constants.arrayImageEkorIkan
import com.ta.fishid.Constants.arrayImageMulutIkan
import com.ta.fishid.Constants.arrayImageSiripIkan
import com.ta.fishid.Constants.arrayMulutIkan
import com.ta.fishid.Constants.arraySiripIkan
import com.ta.fishid.R
import com.ta.fishid.databinding.ListHomePostBinding
import com.ta.fishid.model.PostData
import com.ta.fishid.util.OnLoadMoreListener
import com.ta.fishid.util.Utility
import com.ta.fishid.util.Utility.toastShort
import kotlinx.android.synthetic.main.dialog_info.*
import kotlinx.android.synthetic.main.dialog_report.*

class HomeAdapter(val homeFragment: HomeFragment, val recyclerView: RecyclerView) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var data: MutableList<PostData> = arrayListOf()
    var lastVisibleItem = 0
    var totalItem = 0
    var isLoading = false
    var isComplete = false
    var onLoadMoreListener: OnLoadMoreListener? = null
    val visibleThresHold = 5

    init {
        val linearLayoutManager = recyclerView.layoutManager as LinearLayoutManager
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                totalItem = linearLayoutManager.itemCount
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition()

                if (!isComplete && !isLoading && totalItem < lastVisibleItem + visibleThresHold && dy > 0) {
                    if (onLoadMoreListener != null) {
                        onLoadMoreListener!!.onLoadMore()
                    }
                    isLoading = true
                }
            }
        })
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ItemHomePostHolder(
            ListHomePostBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is ItemHomePostHolder -> holder.bind(data[position])
        }
    }

    fun setOnloadMoreListener(onLoadMoreListener: OnLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener
    }

    inner class ItemHomePostHolder(itemView: ListHomePostBinding) :
        RecyclerView.ViewHolder(itemView.root) {
        val binding = itemView
        fun bind(data: PostData) {
            binding.tvUsername.text = data.username
            binding.tvLike.text = "likes ${data.likeCount}"

            binding.tvCaption.text = SpannableStringBuilder()
                .bold { append(data.username) }
                .append(" ${data.caption}")

            if (data.commentCount > 0 && data.commentStatus) {
                binding.tvComment.text = "View all ${data.commentCount} comments"
                binding.tvComment.visibility = View.VISIBLE
            } else binding.tvComment.visibility = View.GONE

            binding.tvUsername.setOnClickListener {
                homeFragment.profile(data)
            }

            binding.tvComment.setOnClickListener {
                homeFragment.comment(data)
            }

            binding.tvDate.text =
                Utility.getHomeDifferenceTime(Utility.convertStringToDate(3, data.tanggal).time)

            if (data.userId.toString() == homeFragment.viewModel!!.preferencesHelper.getAccountRx().blockingGet().userId) {
                binding.tvReport.visibility = View.GONE
                binding.tvDelete.visibility = View.VISIBLE
                binding.tvDelete.setOnClickListener {
                    Utility.showCustomPromptDialog(homeFragment.requireContext(), "Delete Post?",
                        {
                            it.dismiss()
                            homeFragment.delete(data.postId, adapterPosition)
                        }, {
                            it.dismiss()
                        }
                    )
                }
            } else {
                binding.tvReport.visibility = View.VISIBLE
                binding.tvDelete.visibility = View.GONE
                binding.tvReport.setOnClickListener {
                    Utility.showReportDialog(homeFragment.requireContext()) {
                        it.dismiss()
                        when {
                            it.layout_wrong_info.tag == "1" -> homeFragment.report(
                                data.postId,
                                "Wrong Information",
                                adapterPosition
                            )
                            it.layout_no_fish.tag == "1" -> homeFragment.report(
                                data.postId,
                                "No Fish Related",
                                adapterPosition
                            )
                            it.layout_other.tag == "1" -> homeFragment.report(
                                data.postId, "Other",
                                adapterPosition
                            )
                            else -> toastShort(homeFragment.requireContext(), "No Option Selected")
                        }
                    }
                }
            }

            binding.btLike.setOnClickListener {
                if (!data.likeStatus) {
                    data.likeStatus = true
                    homeFragment.likePost(data.postId.toString())
                    binding.tvLike.text = "likes ${++data.likeCount}"
                } else {
                    data.likeStatus = false
                    homeFragment.unlikePost(data.postId.toString())
                    binding.tvLike.text = "likes ${--data.likeCount}"
                }
                homeFragment.adapter.notifyItemChanged(adapterPosition)
            }

            binding.btComment.setOnClickListener {
                homeFragment.comment(data)
            }

            Glide.with(homeFragment)
                .load(Constants.BaseURL + data.profilePict)
                .error(R.drawable.ic_account)
                .into(binding.circleImageView)

            setScanTag(data)
            setInfoDialog(data)
            binding.data = data
        }

        private fun setInfoDialog(data: PostData) {
            val dialog =
                Utility.showInfoDialog(
                    homeFragment.requireActivity()
                )

            binding.imgInfo.setOnClickListener {
                dialog.show()
                dialog.tv_body.text = data.bentukTubuh
                dialog.tv_mouth.text = data.bentukMulut
                dialog.tv_tail.text = data.bentukEkor
                dialog.tv_fin.text = data.bentukSirip
                dialog.tv_pattern.text = data.corakTubuh

                if (arrayBentukIkan.indexOf(data.bentukTubuh) != -1)
                    dialog.img_body.setImageResource(
                        arrayImageBentukIkan[arrayBentukIkan.indexOf(
                            data.bentukTubuh
                        )]!!
                    )
                else {
                    dialog.img_body.visibility = View.GONE
                    dialog.tv_body.text = "-"
                }

                if (arrayMulutIkan.indexOf(data.bentukMulut) != -1)
                    dialog.img_mouth.setImageResource(
                        arrayImageMulutIkan[arrayMulutIkan.indexOf(
                            data.bentukMulut
                        )]!!
                    )
                else {
                    dialog.img_mouth.visibility = View.GONE
                    dialog.tv_mouth.text = "-"
                }

                if (arrayEkorIkan.indexOf(data.bentukEkor) != -1)
                    dialog.img_tail.setImageResource(
                        arrayImageEkorIkan[arrayEkorIkan.indexOf(
                            data.bentukEkor
                        )]!!
                    )
                else {
                    dialog.img_tail.visibility = View.GONE
                    dialog.tv_tail.text = "-"
                }

                if (arraySiripIkan.indexOf(data.bentukSirip) != -1)
                    dialog.img_fin.setImageResource(
                        arrayImageSiripIkan[arraySiripIkan.indexOf(
                            data.bentukSirip
                        )]!!
                    )
                else {
                    dialog.img_fin.visibility = View.GONE
                    dialog.tv_fin.text = "-"
                }

                if (arrayCorakIkan.indexOf(data.corakTubuh) != -1)
                    dialog.img_pattern.setImageResource(
                        arrayImageCorakIkan[arrayCorakIkan.indexOf(
                            data.corakTubuh
                        )]!!
                    )
                else {
                    dialog.img_pattern.visibility = View.GONE
                    dialog.tv_pattern.text = "-"
                }
            }
        }

        private fun setScanTag(data: PostData) {
            var intrinsicWidth: Int? = 0
            var intrinsicHeight: Int? = 0

            Glide.with(homeFragment)
                .asBitmap()
                .load(Constants.BaseURL + data.image)
                .listener(object : RequestListener<Bitmap> {
                    override fun onLoadFailed(
                        e: GlideException?,
                        model: Any?,
                        target: Target<Bitmap>?,
                        isFirstResource: Boolean
                    ): Boolean {
                        return false
                    }

                    override fun onResourceReady(
                        resource: Bitmap?,
                        model: Any?,
                        target: Target<Bitmap>?,
                        dataSource: DataSource?,
                        isFirstResource: Boolean
                    ): Boolean {
                        val imageView = binding.imgPost
                        val width = imageView.measuredWidth
                        val height =
                            width * resource!!.toDrawable(homeFragment.resources).intrinsicHeight / resource.toDrawable(
                                homeFragment.resources
                            ).intrinsicWidth
                        if (imageView.layoutParams.height != height) {
                            imageView.layoutParams.height = height
                            imageView.requestLayout()
                        }
                        return false
                    }
                })
                .error(R.drawable.ic_account)
                .into(object : CustomTarget<Bitmap>() {
                    override fun onResourceReady(
                        resource: Bitmap,
                        transition: Transition<in Bitmap>?
                    ) {
                        binding.imgPost.setImageBitmap(resource)
                        intrinsicWidth = binding.imgPost.drawable?.intrinsicWidth
                        intrinsicHeight = binding.imgPost.drawable?.intrinsicHeight
                    }

                    override fun onLoadCleared(placeholder: Drawable?) {}
                })

            binding.imgPostTag.visibility = View.GONE
            binding.imgScan.setImageResource(R.drawable.ic_scan_dark)
            if (data.species != "Other") {
                var scanSelected = false
                binding.imgScan.visibility = View.VISIBLE
                binding.imgScan.setOnClickListener {
                    scanSelected = !scanSelected
                    if (scanSelected)
                        binding.imgScan.setImageResource(R.drawable.ic_scan_blue)
                    else
                        binding.imgScan.setImageResource(R.drawable.ic_scan_dark)
                    if (binding.imgPostTag.visibility == View.VISIBLE) {
                        binding.imgPostTag.visibility = View.GONE
                    } else {

                        val tempBitmap = Bitmap.createBitmap(
                            binding.imgPost.width,
                            binding.imgPost.height,
                            Bitmap.Config.ARGB_8888
                        )

                        val text = "${data.species!!.capitalize().replace(
                            "_",
                            " "
                        )}: ${data.probability.toInt()}"


                        val tempCanvas = Canvas(tempBitmap)
                        val paint = Paint(Paint.ANTI_ALIAS_FLAG)
                        paint.style = Paint.Style.STROKE
                        paint.strokeWidth = 5f
                        paint.color = Color.RED
                        val paintText = Paint()
                        paintText.color = Color.BLACK
                        paintText.textSize = 25f
                        val textWidth = paintText.measureText(text)
                        val textHeight = paintText.textSize
                        var rectF =
                            RectF(
                                (data.x1 * binding.imgPost.width / intrinsicWidth!!).toFloat(),
                                (data.y1 * binding.imgPost.height / intrinsicHeight!!).toFloat(),
                                (data.x2 * binding.imgPost.width / intrinsicWidth!!).toFloat(),
                                (data.y2 * binding.imgPost.height / intrinsicHeight!!).toFloat()
                            )
                        tempCanvas.drawRect(rectF, paint)
                        rectF = RectF(
                            (data.x1 * binding.imgPost.width / intrinsicWidth!!).toFloat(),
                            (data.y1 * binding.imgPost.height / intrinsicHeight!!).toFloat(),
                            (data.x1 * binding.imgPost.width / intrinsicWidth!!).toFloat() + textWidth,
                            (data.y1 * binding.imgPost.height / intrinsicHeight!!).toFloat() + textHeight
                        )
                        paint.style = Paint.Style.FILL
                        paint.color = Color.LTGRAY
                        tempCanvas.drawRect(rectF, paint)
                        tempCanvas.drawText(
                            text,
                            (data.x1 * binding.imgPost.width / intrinsicWidth!!).toFloat(),
                            (data.y1 * binding.imgPost.height / intrinsicHeight!!).toFloat() + 20f,
                            paintText
                        )
                        binding.imgPostTag.setImageDrawable(
                            BitmapDrawable(
                                homeFragment.resources,
                                tempBitmap
                            )
                        )
                        binding.imgPostTag.visibility = View.VISIBLE
                    }
                }
            } else binding.imgScan.visibility = View.GONE
        }
    }
}