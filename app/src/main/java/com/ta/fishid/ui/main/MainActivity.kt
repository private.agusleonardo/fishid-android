package com.ta.fishid.ui.main

import android.Manifest
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.content.pm.PackageManager.PERMISSION_GRANTED
import android.database.Cursor
import android.graphics.PorterDuff
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.ActionMode
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import androidx.annotation.ColorRes
import androidx.annotation.RequiresApi
import androidx.core.animation.addListener
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.ta.fishid.Constants
import com.ta.fishid.R
import com.ta.fishid.api.model.Status
import com.ta.fishid.databinding.ActivityMainBinding
import com.ta.fishid.ui.base.BaseActivity
import com.ta.fishid.ui.collection.CollectionFragment
import com.ta.fishid.ui.explore.ExploreFragment
import com.ta.fishid.ui.home.HomeFragment
import com.ta.fishid.ui.post.PostActivity
import com.ta.fishid.ui.profile.ProfileFragment
import com.ta.fishid.util.PhotoController
import com.ta.fishid.util.PhotoInterface
import com.ta.fishid.util.Utility
import com.ta.fishid.util.Utility.toastShort
import id.zelory.compressor.Compressor
import kotlinx.android.synthetic.main.toolbar.*
import java.io.File
import javax.inject.Inject


class MainActivity : BaseActivity() {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var viewModel: MainViewModel

    private val PERMISSION_CODE = 10
    private val PERMISSION_NEEDED = arrayOf(
        Manifest.permission.CAMERA,
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.READ_EXTERNAL_STORAGE
    )
    var postType = ""

    private lateinit var binding: ActivityMainBinding
    val animSetXY = AnimatorSet()
    var animShowing = false
    var fishId = ""

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        viewModel = initViewModel(MainViewModel::class.java, viewModelFactory)

        fishId = intent.getStringExtra("fish_id") ?: ""
        setupViewPager(PagerAdapter(supportFragmentManager))
        setToolbar("FishID", false)
        initObserver()
        initListener()

    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun initListener() {
        binding.menuHome.setOnClickListener {
            if (animShowing) {
                hideScanOption()
                animShowing = false
            }
            viewModel.currentPageLiveData!!.value = 0
        }

        binding.menuExplore.setOnClickListener {
            if (animShowing) {
                hideScanOption()
                animShowing = false
            }
            viewModel.currentPageLiveData!!.value = 1
        }

        binding.menuScan.setOnClickListener {
            animShowing = if (animShowing) {
                hideScanOption()
                false
            } else {
                showScanOption()
                true
            }
        }

        binding.menuCollection.setOnClickListener {
            if (animShowing) {
                hideScanOption()
                animShowing = false
            }
            viewModel.currentPageLiveData!!.value = 2
        }

        binding.menuProfile.setOnClickListener {
            if (animShowing) {
                hideScanOption()
                animShowing = false
            }
            viewModel.currentPageLiveData!!.value = 3
        }

        binding.btCamera.setOnClickListener {
            hideScanOption()
            animShowing = false
            postType = "camera"
            if (checkPermit())
                startActivity(Intent(this, PostActivity::class.java).putExtra("type", postType))
        }
        binding.btGallery.setOnClickListener {
            hideScanOption()
            animShowing = false
            postType = "gallery"
            if (checkPermit())
                startActivity(Intent(this, PostActivity::class.java).putExtra("type", postType))
        }
    }

    private fun initObserver() {

        with(viewModel.currentPageLiveData!!) {
            value = intent.getIntExtra("page", 0)

            observe(this@MainActivity, Observer<Int> { currentPage: Int? ->
                when (currentPage) {
                    0 -> setMenu(currentPage, "Home")
                    1 -> setMenu(currentPage, "Explore")
                    2 -> setMenu(currentPage, "Collection")
                    3 -> setMenu(currentPage, "Profile")
                }
            })
        }
    }

    private fun setMenu(currentItem: Int, title: String) {
        binding.viewPager.setCurrentItem(currentItem, false)
        when (currentItem) {
            0 -> {
                resetActiveMenu()
                binding.imgHome.setBackgroundResource(R.drawable.ic_home_blue_24dp)
                binding.tvHome.setTextColor(resources.getColor(R.color.colorPrimaryDark))
                toolbar.visibility = View.VISIBLE
                toolbar.title = title
            }
            1 -> {
                resetActiveMenu()
                binding.imgExplore.setBackgroundResource(R.drawable.ic_explore_blue_24dp)
                binding.tvExplore.setTextColor(resources.getColor(R.color.colorPrimaryDark))
                toolbar.visibility = View.GONE
                toolbar.title = title
            }
            2 -> {
                resetActiveMenu()
                binding.imgCollection.setBackgroundResource(R.drawable.ic_collection_blue)
                binding.tvCollection.setTextColor(resources.getColor(R.color.colorPrimaryDark))
                toolbar.visibility = View.GONE
                toolbar.title = title
            }
            3 -> {
                resetActiveMenu()
                binding.imgProfile.setBackgroundResource(R.drawable.ic_person_blue_24dp)
                binding.tvProfile.setTextColor(resources.getColor(R.color.colorPrimaryDark))
                toolbar.visibility = View.GONE
            }

        }

    }


    private fun setupViewPager(adapter: PagerAdapter) {
        binding.viewPager.offscreenPageLimit = 2
        binding.viewPager.isPagingEnabled = false
        binding.viewPager.adapter = adapter
    }

    inner class PagerAdapter(fm: FragmentManager) :
        FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
        private var tabItems = 4

        override fun getItem(pos: Int): Fragment {
            return when (pos) {
                0 -> HomeFragment()
                1 -> ExploreFragment(this@MainActivity)
                2 -> CollectionFragment()
                3 -> ProfileFragment()
                else -> Fragment()
            }
        }

        override fun getCount(): Int {
            return tabItems
        }
    }

    private fun showScanOption() {
        val xCamera = ObjectAnimator.ofFloat(binding.btCamera, View.TRANSLATION_X, -100f).apply {
            duration = 100
        }
        val yCamera = ObjectAnimator.ofFloat(binding.btCamera, View.TRANSLATION_Y, -160f).apply {
            duration = 100
        }
        val xGallery = ObjectAnimator.ofFloat(binding.btGallery, View.TRANSLATION_X, 100f).apply {
            duration = 100
        }
        val yGallery = ObjectAnimator.ofFloat(binding.btGallery, View.TRANSLATION_Y, -160f).apply {
            duration = 100
        }
        animSetXY.playTogether(xCamera, yCamera, xGallery, yGallery)
        animSetXY.start()
    }

    private fun hideScanOption() {
        animSetXY.reverse()
    }

    private fun resetActiveMenu() {
        binding.imgHome.setBackgroundResource(R.drawable.ic_home_black_24dp)
        binding.tvHome.setTextColor(resources.getColor(R.color.grey_700))
        binding.imgExplore.setBackgroundResource(R.drawable.ic_explore_black_24dp)
        binding.tvExplore.setTextColor(resources.getColor(R.color.grey_700))
        binding.imgCollection.setBackgroundResource(R.drawable.ic_collection)
        binding.tvCollection.setTextColor(resources.getColor(R.color.grey_700))
        binding.imgProfile.setBackgroundResource(R.drawable.ic_person_black_24dp)
        binding.tvProfile.setTextColor(resources.getColor(R.color.grey_700))
    }

    private fun checkPermit(): Boolean {
        var permissionRequest: Array<String> = arrayOf()

        for (String in PERMISSION_NEEDED) {
            if (ActivityCompat.checkSelfPermission(
                    this,
                    String
                ) != PERMISSION_GRANTED
            ) {
                permissionRequest += String
            }
        }

        if (permissionRequest.isNotEmpty()) {
            ActivityCompat.requestPermissions(this, permissionRequest, PERMISSION_CODE)
        } else {
            return true
        }
        return false
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            PERMISSION_CODE -> {
                var pass = true
                grantResults.forEach {
                    if (it != PERMISSION_GRANTED)
                        pass = false
                }
                if (pass)
                    startActivity(Intent(this, PostActivity::class.java).putExtra("type", postType))
            }
            else -> {
                toastShort(this, "You need to allow the permissions")
            }
        }
    }
}
