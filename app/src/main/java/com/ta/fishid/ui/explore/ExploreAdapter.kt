package com.ta.fishid.ui.explore

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ta.fishid.databinding.ItemFilterFishBinding
import com.ta.fishid.databinding.ItemProfilePostBinding
import com.ta.fishid.model.FilterData
import com.ta.fishid.model.PostData
import com.ta.fishid.util.OnLoadMoreListener

class ExploreAdapter(
    val exploreFragment: ExploreFragment,
    val type: String,
    val recyclerView: RecyclerView?
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var dataFilter: MutableList<FilterData> = arrayListOf()
    var dataExplore: MutableList<PostData> = arrayListOf()
    var lastVisibleItem = 0
    var totalItem = 0
    var isLoading = false
    var isComplete = false
    var onLoadMoreListener: OnLoadMoreListener? = null
    val visibleThresHold = 5

    init {
        if (recyclerView != null) {
            val layoutManager = recyclerView.layoutManager as GridLayoutManager
            recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)

                    totalItem = layoutManager.itemCount
                    lastVisibleItem = layoutManager.findLastVisibleItemPosition()

                    if (!isComplete && !isLoading && totalItem < lastVisibleItem + visibleThresHold && dy > 0) {
                        if (onLoadMoreListener != null) {
                            onLoadMoreListener!!.onLoadMore()
                        }
                        isLoading = true
                    }
                }
            })
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (type) {
            "Filter" -> ItemExploreFilterHolder(
                ItemFilterFishBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false
                )
            )
            else -> ItemExplorePostHolder(
                ItemProfilePostBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false
                )
            )
        }
    }

    override fun getItemCount(): Int {
        return when (type) {
            "Filter" -> dataFilter.size
            else -> dataExplore.size
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is ItemExploreFilterHolder -> holder.bind(dataFilter[position])
            is ItemExplorePostHolder -> holder.bind(dataExplore[position])
        }
    }

    fun setOnloadMoreListener(onLoadMoreListener: OnLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener
    }

    inner class ItemExploreFilterHolder(itemView: ItemFilterFishBinding) :
        RecyclerView.ViewHolder(itemView.root) {
        val binding = itemView
        fun bind(data: FilterData) {
            binding.constraintLayout.setOnClickListener {
                exploreFragment.selectFilter(data)
            }
            if (data.namaLatin != "Other") {
                binding.tvNamaLatin.visibility = View.VISIBLE
                binding.tvNamaLatin.text = "( ${data.namaLatin} )"
            } else binding.tvNamaLatin.visibility = View.GONE
            binding.data = data
        }
    }

    inner class ItemExplorePostHolder(itemView: ItemProfilePostBinding) :
        RecyclerView.ViewHolder(itemView.root) {
        val binding = itemView
        fun bind(data: PostData) {
            binding.imageView.setOnClickListener {
                exploreFragment.intentPostDetail(data.postId.toString())
            }
            binding.data = data
        }
    }
}