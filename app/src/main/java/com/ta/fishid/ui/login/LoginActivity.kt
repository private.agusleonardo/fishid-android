package com.ta.fishid.ui.login

import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.ta.fishid.R
import com.ta.fishid.api.model.Status
import com.ta.fishid.databinding.ActivityLoginBinding
import com.ta.fishid.ui.base.BaseActivity
import com.ta.fishid.ui.main.MainActivity
import org.json.JSONObject
import javax.inject.Inject

class LoginActivity : BaseActivity() {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var viewModel: LoginViewModel

    private lateinit var binding: ActivityLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        viewModel = initViewModel(LoginViewModel::class.java, viewModelFactory)
        binding.viewModel = viewModel

        setLoadingDialog(this)
        initObserver()
        initListener()
    }

    private fun initListener() {
        binding.etPass.setOnKeyListener { v, keyCode, event ->
            when (keyCode) {
                KeyEvent.KEYCODE_ENTER -> loginCheck()
            }
            false
        }
        binding.btLogin.setOnClickListener {
            loginCheck()
        }
        binding.tvRegister.setOnClickListener {
            startActivity(Intent(this, RegisterActivity::class.java))
        }
    }

    private fun loginCheck() {
        if (!binding.etUsername.text.isNullOrEmpty() || !binding.etPass.text.isNullOrEmpty()) {
            viewModel.username.set(binding.etUsername.text.toString())
            viewModel.password.set(binding.etPass.text.toString())
            binding.tvMessage.visibility = View.GONE
            viewModel.login()
        } else showMessage("Username and password must not empty")
//        startActivity(Intent(this, MainActivity::class.java))
    }

    private fun showMessage(text: String) {
        binding.tvMessage.visibility = View.VISIBLE
        binding.tvMessage.text = text
    }

    private fun initObserver() {
        viewModel.loginCheck.observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    if (it.data != null) {
                        dismissDialog()
                        startActivity(Intent(this, MainActivity::class.java))
                        finish()
                    }
                }
                Status.VALIDATION_ERROR -> {
                    val json = JSONObject(it.message!!)
                    dismissDialog()
                    showMessage(json.getString("message"))
                }
                Status.ERROR -> {
                    dismissDialog()
                    Toast.makeText(this, "Server Error", Toast.LENGTH_SHORT).show()
                }
                Status.LOADING -> showDialog()
            }
        })
    }
}