package com.ta.fishid.ui.profile

import android.app.Activity
import android.content.Intent
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.ta.fishid.Constants
import com.ta.fishid.R
import com.ta.fishid.api.model.Status
import com.ta.fishid.databinding.ActivityEditProfileBinding
import com.ta.fishid.ui.base.BaseActivity
import com.ta.fishid.util.PhotoController
import com.ta.fishid.util.PhotoInterface
import id.zelory.compressor.Compressor
import org.json.JSONObject
import java.io.File
import java.util.*
import javax.inject.Inject

class EditProfileActivity : BaseActivity(), PhotoInterface {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var viewModel: ProfileViewModel
    private lateinit var binding: ActivityEditProfileBinding

    lateinit var photoController: PhotoController
    lateinit var mCurrentPhotoPath: String
    var cal = Calendar.getInstance()

    var profilePicture: File? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_edit_profile)
        viewModel = initViewModel(ProfileViewModel::class.java, viewModelFactory)
        binding.viewModel = viewModel

        viewModel.username.set(viewModel.preferencesHelper.getAccountRx().blockingGet().username)
        viewModel.bio.set(viewModel.preferencesHelper.getAccountRx().blockingGet().bio)
        viewModel.picture.set(Constants.BaseURL + viewModel.preferencesHelper.getAccountRx().blockingGet().profilePict)

        photoController = PhotoController(this, this)

        setToolbar("Edit Profile", true)
        initListener()
        initObserver()
    }

    private fun initObserver() {
        viewModel.checkPostProfile.observe(this, androidx.lifecycle.Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    if (it.data != null) {
                        viewModel.preferencesHelper.saveAccount(it.data)
                    }
                    Toast.makeText(this, "Profile updated", Toast.LENGTH_SHORT).show()
                    finish()
                }
                Status.VALIDATION_ERROR -> {
                    val json = JSONObject(it.message!!)
                    showMessage(json.getString("message"))
                }
                Status.ERROR -> {
                    Toast.makeText(this, "Server Error", Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_save, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        var pass1 = false
        var pass2 = false
        viewModel.username.set(binding.etUsername.text.toString())
        viewModel.bio.set(binding.etEmail.text.toString())
        if (binding.etUsername.text.matches(Regex("^[a-zA-Z0-9._-]{3,}\$")))
            if (binding.etUsername.text.count() < 6 || binding.etUsername.text.count() > 20)
                showMessage("Your username must be between 6 and 20 characters")
            else pass1 = true
        else showMessage("Username must not contains special character or whitespace")

        if (binding.etEmail.text.count() >= 50)
            showMessage("Bio must not exceed 50 characters")
        else pass2 = true

        if (pass1 && pass2) {
            if (profilePicture != null) viewModel.postEditProfile(profilePicture!!)
            else viewModel.postProfile()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun showMessage(text: String) {
        binding.tvMessage.visibility = View.VISIBLE
        binding.tvMessage.text = text
    }

    private fun initListener() {
        binding.tvPhoto.setOnClickListener {
            photoController.dialogAttachment()
        }
        binding.tvGantiPassword.setOnClickListener {
            startActivity(Intent(this, ChangePasswordActivity::class.java))
        }
    }

    override fun path(path: String) {
        mCurrentPhotoPath = path
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        try {
            if (requestCode == Constants.REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) {
                val file = File(mCurrentPhotoPath)

                val compressedImageBitmap = Compressor(this).compressToFile(file)
                profilePicture = compressedImageBitmap

                loadProfileImage()
            }

            if (requestCode == Constants.GALLERY_IMAGE && resultCode == Activity.RESULT_OK) {
                val selectedImage: Uri = data!!.data!!
                val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)
                val cursor: Cursor? = contentResolver.query(
                    selectedImage,
                    filePathColumn, null, null, null
                )
                if (cursor == null || cursor.count < 1) {
                    Toast.makeText(this, "Garmbar tidak terpilih", Toast.LENGTH_SHORT).show()
                } else {
                    cursor.moveToFirst()
                    val columnIndex: Int = cursor.getColumnIndex(filePathColumn[0])
                    if (columnIndex < 0)
                        return Toast.makeText(this, "Tidak ada gambar", Toast.LENGTH_SHORT).show()
                    val picturePath: String = cursor.getString(columnIndex)
                    cursor.close()

                    val file = File(picturePath)

                    val compressedImageBitmap = Compressor(this).compressToFile(file)
                    profilePicture = compressedImageBitmap
                    loadProfileImage()
                }
            }

        } catch (io: Exception) {
            io.printStackTrace()
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun loadProfileImage() {
        Glide.with(this)
            .asBitmap()
            .load(profilePicture)
            .into(object : CustomTarget<Bitmap>() {
                override fun onResourceReady(
                    resource: Bitmap,
                    transition: Transition<in Bitmap>?
                ) {
                    binding.imgProfile.setImageBitmap(resource)
                }

                override fun onLoadCleared(placeholder: Drawable?) {}
            })
    }
}