package com.ta.fishid.ui.login

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.ta.fishid.api.model.Resource
import com.ta.fishid.cache.PreferencesHelper
import com.ta.fishid.model.UserData
import com.ta.fishid.repository.UserRepository
import com.ta.fishid.ui.base.BaseViewModel
import javax.inject.Inject

class LoginViewModel @Inject constructor(
    val userRepository: UserRepository,
    val preferencesHelper: PreferencesHelper
) : BaseViewModel() {
    var name = ObservableField("")
    var username = ObservableField("")
    var bio = ObservableField("")
    var password = ObservableField("")
    var loginTrigger: MutableLiveData<Boolean> = MutableLiveData()
    var registerTrigger: MutableLiveData<Boolean> = MutableLiveData()

    var loginCheck: LiveData<Resource<UserData>> = Transformations.switchMap(loginTrigger) {
        userRepository.login(username.get()!!, password.get()!!)
    }

    fun login() {
        loginTrigger.value = loginTrigger.value == false
    }

    var registerCheck: LiveData<Resource<UserData>> = Transformations.switchMap(registerTrigger) {
        userRepository.register(username.get()!!, bio.get()!!, password.get()!!)
    }

    fun register() {
        registerTrigger.value = registerTrigger.value == false
    }

    fun postFcmToken() {
        userRepository.postFcmToken(
            preferencesHelper.getAccountRx().blockingGet().username ?: "",
            preferencesHelper.getFcmToken()
        )
    }

}