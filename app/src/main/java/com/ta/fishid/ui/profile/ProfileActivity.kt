package com.ta.fishid.ui.profile

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.ta.fishid.Constants
import com.ta.fishid.R
import com.ta.fishid.api.model.Status
import com.ta.fishid.databinding.ActivityProfileBinding
import com.ta.fishid.model.PostData
import com.ta.fishid.ui.base.BaseActivity
import com.ta.fishid.ui.postDetail.PostDetailActivity
import com.ta.fishid.util.OnLoadMoreListener
import com.ta.fishid.util.Utility
import javax.inject.Inject

class ProfileActivity : BaseActivity() {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var viewModel: ProfileViewModel
    private lateinit var binding: ActivityProfileBinding
    lateinit var adapter: ProfileAdapter
    var position = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_profile)
        viewModel = initViewModel(ProfileViewModel::class.java, viewModelFactory)
        binding.viewModel = viewModel

        setToolbar(intent.getStringExtra("username"), true)
        initObserver()
        initAdapter()
        initListener()

        viewModel.userId.set(intent.getStringExtra("user_id"))
        if (viewModel.userId.get() != viewModel.preferencesHelper.getAccountRx().blockingGet().userId)
            binding.btEdit.visibility = View.GONE
        viewModel.getProfile()
    }

    private fun initAdapter() {
        binding.recyclerView.layoutManager = GridLayoutManager(this, 3)
        adapter = ProfileAdapter(null, this, binding.recyclerView)
        binding.recyclerView.adapter = adapter
    }

    private fun initObserver() {
        viewModel.checkGetProfile.observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    if (it.data != null) {
                        if (viewModel.page.get() == 0) {
                            adapter.isComplete = false
                            adapter.data.clear()
                        }
                        if (it.data.post.size < viewModel.perPage.get()) adapter.isComplete = true
                        adapter.data.addAll(it.data.post)
                        adapter.notifyDataSetChanged()
                        viewModel.setRefreshing(false)
                        viewModel.username.set(it.data.user.username)
                        viewModel.bio.set(it.data.user.bio)
                        viewModel.picture.set(Constants.BaseURL + it.data.user.profilePict)

                        adapter.isLoading = false
                    }
                }
                Status.LOADING -> {
                    adapter.isLoading = true
                }
                Status.ERROR -> {
                    Utility.toastShort(this, "Something went wrong")
                    viewModel.setRefreshing(false)
                }
            }
        })
    }

    private fun initListener() {
        binding.btEdit.setOnClickListener {
            startActivity(Intent(this, EditProfileActivity::class.java))
        }

        adapter.setOnloadMoreListener(object : OnLoadMoreListener {
            override fun onLoadMore() {
                viewModel.page.set(viewModel.page.get() + 1)
                viewModel.getProfile()
            }
        })
    }

    fun intentPostDetail(data: PostData, position: Int) {
        this.position = position
        startActivityForResult(
            Intent(
                this,
                PostDetailActivity::class.java
            ).putExtra("post_id", data.postId.toString()).putExtra(
                "username",
                data.username
            ),
            1
        )
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
            viewModel.offset.set(viewModel.offset.get() + 1)
            adapter.data.removeAt(position)
            adapter.notifyItemRemoved(position)
        }
    }

}