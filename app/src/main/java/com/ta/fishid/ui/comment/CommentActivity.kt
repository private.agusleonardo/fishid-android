package com.ta.fishid.ui.comment

import android.content.Intent
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.view.View
import androidx.core.text.bold
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.ta.fishid.Constants
import com.ta.fishid.R
import com.ta.fishid.api.model.Status
import com.ta.fishid.databinding.ActivityCommentBinding
import com.ta.fishid.model.Comments
import com.ta.fishid.ui.base.BaseActivity
import com.ta.fishid.ui.profile.ProfileActivity
import javax.inject.Inject

class CommentActivity : BaseActivity() {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var viewModel: CommentViewModel

    private lateinit var binding: ActivityCommentBinding
    lateinit var adapter: CommentAdapter
    var postId = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_comment)
        viewModel = initViewModel(CommentViewModel::class.java, viewModelFactory)
        binding.viewModel = viewModel

        Glide.with(this)
            .load(Constants.BaseURL + intent.getStringExtra("profile_pict"))
            .error(R.drawable.ic_account)
            .into(binding.circleImageView)
        binding.tvUsername.text = SpannableStringBuilder()
            .bold { append(intent.getStringExtra("username")) }
            .append(" ${intent.getStringExtra("caption")}")

        setToolbar("Komentar", true)
        setLoadingDialog(this)

        initObserver()
        initAdapter()
        initListener()

        postId = intent.getIntExtra("post_id", 0)
        viewModel.getComment(postId.toString())
    }

    private fun initObserver() {
        viewModel.checkGetComment.observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    if (it.data != null) {
                        if (it.data.userId.toString() == viewModel.preferencesHelper.getAccountRx().blockingGet().userId) {
                            binding.switch1.visibility = View.VISIBLE
                            binding.switch1.isChecked = !it.data.commentStatus
                        } else
                            binding.switch1.visibility = View.GONE
                        if (it.data.commentStatus) {
                            binding.layoutComment.visibility = View.VISIBLE
                            binding.layoutCommentDisable.visibility = View.GONE
                        } else {
                            binding.layoutComment.visibility = View.GONE
                            binding.layoutCommentDisable.visibility = View.VISIBLE
                        }

                        adapter.data.clear()
                        adapter.data.addAll(it.data.comments)
                        adapter.notifyDataSetChanged()
                        viewModel.setRefreshing(false)
                    }
                }
            }
        })

        viewModel.checkPostComment.observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    viewModel.getComment(postId.toString())
                }
                Status.ERROR -> {

                }
            }
        })

        viewModel.checkDisableComment.observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                }
                Status.ERROR -> {
                }
            }
        })
    }

    private fun initListener() {
        binding.switch1.setOnClickListener {
            viewModel.disableComment(postId.toString(), !binding.switch1.isChecked)
            if (!binding.switch1.isChecked) {
                binding.layoutComment.visibility = View.VISIBLE
                binding.layoutCommentDisable.visibility = View.GONE
            } else {
                binding.layoutComment.visibility = View.GONE
                binding.layoutCommentDisable.visibility = View.VISIBLE
            }
        }
        binding.btSend.setOnClickListener {
            if (binding.etComment.text.toString().isNotEmpty())
                viewModel.postComment(
                    postId.toString(),
                    binding.etComment.text.toString()
                )
            binding.etComment.setText("")
        }
    }

    private fun initAdapter() {
        adapter = CommentAdapter(this)
        binding.recyclerView.adapter = adapter
    }

    fun profile(data: Comments) {
        startActivity(
            Intent(this, ProfileActivity::class.java)
                .putExtra("user_id", data.userId.toString())
                .putExtra("username", data.username)
        )
    }
}