package com.ta.fishid.ui.home

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.ta.fishid.Constants
import com.ta.fishid.cache.PreferencesHelper
import com.ta.fishid.repository.HomeRepository
import com.ta.fishid.repository.PostRepository
import com.ta.fishid.repository.UserRepository
import com.ta.fishid.ui.base.BaseViewModel
import javax.inject.Inject

class HomeViewModel @Inject constructor(
    val homeRepository: HomeRepository,
    val postRepository: PostRepository,
    val preferencesHelper: PreferencesHelper
) : BaseViewModel() {
    var showLoading = ObservableBoolean(false)
    var showEmpty = ObservableBoolean(false)
    var showError = ObservableBoolean(false)
    var isLoading = ObservableBoolean(false)

    var checkHomeTrigger: MutableLiveData<Boolean> = MutableLiveData()
    var checkLikeTrigger: MutableLiveData<Boolean> = MutableLiveData()
    var checkUnlikeTrigger: MutableLiveData<Boolean> = MutableLiveData()
    var checkReportTrigger: MutableLiveData<Boolean> = MutableLiveData()
    var checkDeleteTrigger: MutableLiveData<Boolean> = MutableLiveData()
    var changeFragment = ObservableBoolean(false)
    var page = ObservableInt(0)
    var perPage = ObservableInt(10)
    var offset = ObservableInt(0)
    var name = ObservableField("")
    var foto = ObservableField("")
    var reason = ObservableField("")
    var postId = ObservableField("")
    var userId = preferencesHelper.getAccountRx().blockingGet().userId ?: ""

    fun logout() {
        preferencesHelper.clearSession()
    }

    fun updateProfile() {
        name.set(preferencesHelper.getAccountRx().blockingGet().username)
        foto.set(Constants.BaseURL + preferencesHelper.getAccountRx().blockingGet().profilePict)
    }

    var checkHome = Transformations.switchMap(checkHomeTrigger) {
        homeRepository.getHome(page.get(), perPage.get(), offset.get())
    }

    fun getHome() {
        checkHomeTrigger.value = checkHomeTrigger.value == false
    }

    var checkLikePost = Transformations.switchMap(checkLikeTrigger) {
        postRepository.likePost(userId, postId.get()!!)
    }

    fun likePost(postId: String) {
        this.postId.set(postId)
        checkLikeTrigger.value = checkLikeTrigger.value == false
    }

    var checkUnlikePost = Transformations.switchMap(checkUnlikeTrigger) {
        postRepository.unlikePost(userId, postId.get()!!)
    }

    fun unlikePost(postId: String) {
        this.postId.set(postId)
        checkUnlikeTrigger.value = checkUnlikeTrigger.value == false
    }

    var checkReport = Transformations.switchMap(checkReportTrigger) {
        postRepository.report(userId, postId.get()!!, reason.get()!!)
    }

    fun report(postId: String, reason: String) {
        this.postId.set(postId)
        this.reason.set(reason)
        checkReportTrigger.value = checkReportTrigger.value == false
    }


    var checkDelete = Transformations.switchMap(checkDeleteTrigger) {
        postRepository.delete(userId, postId.get()!!)
    }

    fun delete(postId: String) {
        this.postId.set(postId)
        checkDeleteTrigger.value = checkDeleteTrigger.value == false
    }

    fun onRefresh() {
        setRefreshing(true)
        page.set(0)
        offset.set(0)
        getHome()
    }

    fun setRefreshing(action: Boolean) {
        isLoading.set(action)
    }

    fun setShowLoading(action: Boolean) {
        showLoading.set(action)
    }

    fun setShowError(action: Boolean) {
        showError.set(action)
    }

    fun setShowEmpty(action: Boolean) {
        showEmpty.set(action)
    }
}