package com.ta.fishid.ui.comment

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.ta.fishid.cache.PreferencesHelper
import com.ta.fishid.repository.PostRepository
import com.ta.fishid.repository.UserRepository
import com.ta.fishid.ui.base.BaseViewModel
import com.ta.fishid.util.Utility
import java.util.*
import javax.inject.Inject

class CommentViewModel @Inject constructor(
    val postRepository: PostRepository,
    val preferencesHelper: PreferencesHelper
) : BaseViewModel() {
    var showLoading = ObservableBoolean(false)
    var showEmpty = ObservableBoolean(false)
    var showError = ObservableBoolean(false)
    var isLoading = ObservableBoolean(false)

    var name = ObservableField("")
    var username = ObservableField("")
    var bio = ObservableField("")
    var password = ObservableField("")
    var postId = ObservableField("")
    var comment = ObservableField("")
    var time = ObservableField("")
    var status = ObservableBoolean(false)
    var checkGetCommentTrigger: MutableLiveData<Boolean> = MutableLiveData()
    var checkPostCommentTrigger: MutableLiveData<Boolean> = MutableLiveData()
    var checkDisableCommentTrigger: MutableLiveData<Boolean> = MutableLiveData()

    var checkGetComment = Transformations.switchMap(checkGetCommentTrigger) {
        postRepository.getComment(postId.get()!!)
    }

    fun getComment(postId: String) {
        this.postId.set(postId)
        checkGetCommentTrigger.value = checkGetCommentTrigger.value == false
    }

    var checkPostComment = Transformations.switchMap(checkPostCommentTrigger) {
        postRepository.postComment(postId.get()!!, comment.get()!!, time.get()!!)
    }

    fun postComment(postId: String, comment: String) {
        this.postId.set(postId)
        this.comment.set(comment)
        time.set(Utility.convertDateToString(3, Calendar.getInstance().time))
        checkPostCommentTrigger.value = checkPostCommentTrigger.value == false
    }

    var checkDisableComment = Transformations.switchMap(checkDisableCommentTrigger) {
        postRepository.disableComment(postId.get()!!,status.get())
    }

    fun disableComment(postId: String, status: Boolean) {
        this.postId.set(postId)
        this.status.set(status)
        checkDisableCommentTrigger.value = checkDisableCommentTrigger.value == false
    }

    fun onRefresh() {
        setRefreshing(true)
        getComment(postId.get()!!)
    }

    fun setRefreshing(action: Boolean) {
        isLoading.set(action)
    }

    fun setShowLoading(action: Boolean) {
        showLoading.set(action)
    }

    fun setShowError(action: Boolean) {
        showError.set(action)
    }

    fun setShowEmpty(action: Boolean) {
        showEmpty.set(action)
    }
}
