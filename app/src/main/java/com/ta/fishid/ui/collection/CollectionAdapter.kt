package com.ta.fishid.ui.collection

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.ta.fishid.Constants
import com.ta.fishid.R
import com.ta.fishid.databinding.ItemHistoryBinding
import com.ta.fishid.databinding.ItemProfilePostBinding
import com.ta.fishid.databinding.ItemSubHistoryBinding
import com.ta.fishid.model.Fish
import com.ta.fishid.model.HistoryData
import com.ta.fishid.model.PostData
import com.ta.fishid.util.Utility

class CollectionAdapter(val activity: CollectionFragment, val type: String) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var dataLike: MutableList<PostData> = arrayListOf()
    var dataHistory: MutableList<HistoryData> = arrayListOf()
    var dataSubHistory: MutableList<Fish> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (type) {
            Constants.Like -> ItemCollectionLikeHolder(
                ItemProfilePostBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false
                )
            )
            Constants.History -> ItemCollectionHistoryHolder(
                ItemHistoryBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false
                )
            )
            else -> ItemCollectionSubHistoryHolder(
                ItemSubHistoryBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false
                )
            )
        }
    }

    override fun getItemCount(): Int {
        return when (type) {
            Constants.Like -> dataLike.size
            Constants.History -> dataHistory.size
            else -> dataSubHistory.size
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is ItemCollectionLikeHolder -> holder.bind(dataLike[position])
            is ItemCollectionHistoryHolder -> holder.bind(dataHistory[position])
            is ItemCollectionSubHistoryHolder -> holder.bind(dataSubHistory[position])
        }
    }

    inner class ItemCollectionLikeHolder(itemView: ItemProfilePostBinding) :
        RecyclerView.ViewHolder(itemView.root) {
        val binding = itemView
        fun bind(data: PostData) {
            binding.imageView.setOnClickListener {
                activity.intentPostDetail(data)
            }
            binding.data = data
        }
    }

    inner class ItemCollectionHistoryHolder(itemView: ItemHistoryBinding) :
        RecyclerView.ViewHolder(itemView.root) {
        val binding = itemView
        lateinit var adapter: CollectionAdapter
        fun bind(data: HistoryData) {
            binding.textView3.text =
                Utility.convertDateToString(10, Utility.convertStringToDate(3, data.waktu))
            binding.textView.text = "You scanned ${data.fish.size} fish species"
            binding.constraintLayout.setOnClickListener {
                data.expanded = !data.expanded
                activity.adapterHistory.notifyItemChanged(adapterPosition)
            }

            adapter = CollectionAdapter(activity, Constants.SubHistory)
            binding.recyclerView.adapter = adapter
            adapter.dataSubHistory.addAll(data.fish)
            adapter.notifyDataSetChanged()

            binding.data = data
        }
    }

    inner class ItemCollectionSubHistoryHolder(itemView: ItemSubHistoryBinding) :
        RecyclerView.ViewHolder(itemView.root) {
        val binding = itemView
        lateinit var adapter: CollectionAdapter
        fun bind(data: Fish) {
            binding.tvNama.text = data.namaLatin
            binding.tvJumlah.text = data.jumlah.toString() + " Fish(es)"
        }
    }
}