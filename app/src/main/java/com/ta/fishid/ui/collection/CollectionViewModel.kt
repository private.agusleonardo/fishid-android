package com.ta.fishid.ui.collection

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.ta.fishid.cache.PreferencesHelper
import com.ta.fishid.repository.CollectionRepository
import com.ta.fishid.repository.UserRepository
import com.ta.fishid.ui.base.BaseViewModel
import javax.inject.Inject

class CollectionViewModel @Inject constructor(
    val collectionRepository: CollectionRepository,
    val preferencesHelper: PreferencesHelper
) : BaseViewModel() {
    var showLoading = ObservableBoolean(false)
    var showEmpty = ObservableBoolean(false)
    var showError = ObservableBoolean(false)
    var isLoading = ObservableBoolean(false)

    var username = ObservableField("")
    var picture = ObservableField("")
    var oldPassword = ObservableField("")
    var newPassword = ObservableField("")

    var checkGetLikeTrigger: MutableLiveData<Boolean> = MutableLiveData()
    var checkGetHistoryTrigger: MutableLiveData<Boolean> = MutableLiveData()

    var checkGetLike = Transformations.switchMap(checkGetLikeTrigger) {
        collectionRepository.getCollectionLike(
            preferencesHelper.getAccountRx().blockingGet().userId!!
        )
    }

    fun getCollectionLike() {
        checkGetLikeTrigger.value = checkGetLikeTrigger.value == false
    }

    var checkGetHistory = Transformations.switchMap(checkGetHistoryTrigger) {
        collectionRepository.getCollectionHistory(
            preferencesHelper.getAccountRx().blockingGet().userId!!
        )
    }

    fun getCollectionHistory() {
        checkGetHistoryTrigger.value = checkGetHistoryTrigger.value == false
    }

    fun onRefresh() {
        setRefreshing(true)
        getCollectionLike()
    }

    fun setRefreshing(action: Boolean) {
        isLoading.set(action)
    }

    fun setShowLoading(action: Boolean) {
        showLoading.set(action)
    }

    fun setShowError(action: Boolean) {
        showError.set(action)
    }

    fun setShowEmpty(action: Boolean) {
        showEmpty.set(action)
    }
}