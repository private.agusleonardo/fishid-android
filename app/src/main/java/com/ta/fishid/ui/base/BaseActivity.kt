package com.ta.fishid.ui.base

import android.app.Activity
import android.app.Dialog
import android.graphics.Color
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import com.faltenreich.skeletonlayout.Skeleton
import com.faltenreich.skeletonlayout.applySkeleton
import com.faltenreich.skeletonlayout.createSkeleton
import com.ta.fishid.util.Utility
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.toolbar.*

abstract class BaseActivity : DaggerAppCompatActivity() {

    private lateinit var dialog: Dialog
    lateinit var skeleton: Skeleton
    lateinit var recyclerSkeleton: Skeleton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    fun setUpSkeleton(view: View, layoutResId: Int) {
        skeleton = view.createSkeleton(layoutResId)
        skeleton.shimmerDurationInMillis = 1500
        skeleton.shimmerColor = Color.parseColor("#eeeeee")
    }

    fun setUpRecyclerSkeleton(recyclerView: RecyclerView, layoutResId: Int, itemCount: Int) {
        recyclerSkeleton = recyclerView.applySkeleton(layoutResId, itemCount)
        recyclerSkeleton.shimmerDurationInMillis = 1500
        recyclerSkeleton.shimmerColor = Color.parseColor("#eeeeee")
    }

    fun setLoadingDialog(activity: Activity) {
        dialog = Utility.dialogLoading(activity)
    }

    fun showDialog() {
        dialog.show()
    }

    fun dismissDialog() {
        dialog.dismiss()
    }

    fun setToolbar(title: String, backAction: Boolean) {
        setSupportActionBar(toolbar)
        supportActionBar!!.title = title
        if (backAction) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.setDisplayShowHomeEnabled(true)
            toolbar.setNavigationOnClickListener {
                onBackPressed()
            }
        }
    }

    fun <T : ViewModel?> initViewModel(
        modelClass: Class<T>,
        isNeedFactory: ViewModelProvider.Factory?
    ): T {
        return if (isNeedFactory != null)
            ViewModelProviders.of(this, isNeedFactory).get(modelClass)
        else
            ViewModelProviders.of(this).get(modelClass)
    }


}