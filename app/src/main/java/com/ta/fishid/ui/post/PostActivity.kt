package com.ta.fishid.ui.post

import android.app.Activity
import android.content.Intent
import android.database.Cursor
import android.graphics.*
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.RadioButton
import android.widget.Toast
import androidx.core.graphics.drawable.toBitmap
import androidx.core.view.size
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.ta.fishid.Constants
import com.ta.fishid.R
import com.ta.fishid.api.model.Status
import com.ta.fishid.databinding.ActivityPostBinding
import com.ta.fishid.model.DetectData
import com.ta.fishid.ui.base.BaseActivity
import com.ta.fishid.ui.login.LoginActivity
import com.ta.fishid.ui.main.MainActivity
import com.ta.fishid.util.PhotoController
import com.ta.fishid.util.PhotoInterface
import com.ta.fishid.util.Utility
import com.ta.fishid.util.Utility.toastShort
import id.zelory.compressor.Compressor
import java.io.File
import javax.inject.Inject


class PostActivity : BaseActivity(), PhotoInterface {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var viewModel: PostViewModel

    private lateinit var binding: ActivityPostBinding
    lateinit var photoController: PhotoController
    lateinit var currentPhotoPath: String

    lateinit var menuSave: MenuItem
    lateinit var adapter: PostAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_post)
        viewModel = initViewModel(PostViewModel::class.java, viewModelFactory)
        binding.viewModel = viewModel
        photoController = PhotoController(this, this)

        setToolbar("Scan", true)

        when (intent.getStringExtra("type")) {
            "camera" -> photoController.cameraOnlyAttachment()
            "gallery" -> photoController.galleryOnlyAttachment()
        }

        setLoadingDialog(this)
        initObserver()
        initSpinner()
        initListener()
    }

    private fun initObserver() {
        viewModel.checkPost.observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    toastShort(this, "Post berhasil")
                    super.onBackPressed()
                }
                Status.ERROR -> {
                    toastShort(this, "Post gagal disimpan")
                }
            }
        })

        viewModel.checkDetectPost.observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    if (it.data != null) {
                        val data = it.data as MutableList<DetectData>
                        data.add(addOtherFish())
                        viewModel.data = data
                        val tempBitmap = Bitmap.createBitmap(
                            binding.imgPost.width,
                            binding.imgPost.height,
                            Bitmap.Config.ARGB_8888
                        )

                        val b = binding.imgPost.drawable?.toBitmap()!!

                        val tempCanvas = Canvas(tempBitmap)
                        val paint = Paint(Paint.ANTI_ALIAS_FLAG)
                        val paintText = Paint()

                        data.forEach {
                            if (it.fishId != "0") {
                                val text = "${it.species.capitalize().replace(
                                    "_",
                                    " "
                                )}: ${it.probability.toInt()}"
                                paint.style = Paint.Style.STROKE
                                paint.strokeWidth = 2f
                                paint.color = Color.RED
                                paintText.color = Color.BLACK
                                paintText.textSize = 25f
                                val textWidth = paintText.measureText(text)
                                val textHeight = paintText.textSize
                                var rectF =
                                    RectF(
                                        (it.x1 * binding.imgPost.width / b.width).toFloat(),
                                        (it.y1 * binding.imgPost.height / b.height).toFloat(),
                                        (it.x2 * binding.imgPost.width / b.width).toFloat(),
                                        (it.y2 * binding.imgPost.height / b.height).toFloat()
                                    )
                                tempCanvas.drawRect(rectF, paint)
                                rectF = RectF(
                                    (it.x1 * binding.imgPost.width / b.width).toFloat(),
                                    (it.y1 * binding.imgPost.height / b.height).toFloat(),
                                    (it.x1 * binding.imgPost.width / b.width).toFloat() + textWidth,
                                    (it.y1 * binding.imgPost.height / b.height).toFloat() + textHeight
                                )
                                paint.style = Paint.Style.FILL
                                paint.color = Color.LTGRAY
                                tempCanvas.drawRect(rectF, paint)
                                tempCanvas.drawText(
                                    text,
                                    (it.x1 * binding.imgPost.width / b.width).toFloat(),
                                    (it.y1 * binding.imgPost.height / b.height).toFloat() + 20f,
                                    paintText
                                )
                            }
                            addRadioItem(it)
                        }

                        binding.imgPostTag.setImageDrawable(
                            BitmapDrawable(
                                resources,
                                tempBitmap
                            )
                        )

                        binding.imgPost.setOnClickListener {
                            binding.imgPostTag.visibility = View.VISIBLE
                        }

                        binding.imgPostTag.setOnClickListener {
                            binding.imgPostTag.visibility = View.GONE
                        }
                    }
                    dismissDialog()
                }
                Status.LOADING -> {
                    showDialog()
                }
                Status.ERROR -> {
                    dismissDialog()
                    toastShort(this, "Detect fish failed")
                }
            }
        })
    }

    private fun addOtherFish(): DetectData {
        return DetectData(
            "0",
            "Other",
            "Other",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            0.0,
            "",
            0,
            0,
            0,
            0,
            false
        )
    }

    private fun initSpinner() {
        val arrayBentukIkan =
            arrayOf(
                "Not selected",
                "Angelfish",
                "Barracuda",
                "Batfish",
                "Butterflyfish",
                "Damselfish",
                "Fusilier",
                "Grouper",
                "Parrotfish",
                "Pufferfish",
                "Snapperfish",
                "Soldierfish",
                "Surgeonfish",
                "Sweetlips",
                "Triggerfish",
                "Wrasse"
            )
        val arrayImageBentukIkan = arrayOf(
            null,
            R.drawable.fish_shapes_angelfish, R.drawable.fish_shapes_barracuda,
            R.drawable.fish_shapes_batfish, R.drawable.fish_shapes_butterflyfish,
            R.drawable.fish_shapes_damselfish, R.drawable.fish_shapes_fusilier,
            R.drawable.fish_shapes_grouper, R.drawable.fish_shapes_parrotfish,
            R.drawable.fish_shapes_pufferfish, R.drawable.fish_shapes_snapperfish,
            R.drawable.fish_shapes_soldierfish, R.drawable.fish_shapes_surgeonfish,
            R.drawable.fish_shapes_sweetlips, R.drawable.fish_shapes_triggerfish,
            R.drawable.fish_shapes_wrasse
        )
        binding.spinnerBadan.adapter = SpinnerAdapter(
            this,
            R.layout.spinner_fish_category,
            arrayBentukIkan,
            arrayImageBentukIkan
        )

        binding.spinnerBadan.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (arrayBentukIkan[position] != "Not selected")
                    viewModel.selectedBentukIkan = arrayBentukIkan[position]
                else
                    viewModel.selectedBentukIkan = ""
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }

        val arrayMulutIkan =
            arrayOf(
                "Not selected",
                "Superior",
                "Terminal",
                "Subterminal",
                "Inferior",
                "Barbels",
                "Tubular",
                "Elongated"
            )
        val arrayImageMulutIkan = arrayOf(
            null,
            R.drawable.fish_mouths_superior, R.drawable.fish_mouths_terminal,
            R.drawable.fish_mouths_subterminal, R.drawable.fish_mouths_inferior,
            R.drawable.fish_mouths_barbels, R.drawable.fish_mouths_tubular,
            R.drawable.fish_mouths_elongated
        )
        binding.spinnerMulut.adapter = SpinnerAdapter(
            this,
            R.layout.spinner_fish_category,
            arrayMulutIkan,
            arrayImageMulutIkan
        )

        binding.spinnerMulut.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (arrayMulutIkan[position] != "Not selected")
                    viewModel.selectedMulutIkan = arrayMulutIkan[position]
                else
                    viewModel.selectedMulutIkan = ""
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }

        val arrayEkorIkan =
            arrayOf(
                "Not selected",
                "Forked",
                "Truncate",
                "Rounded",
                "Lunate",
                "Lanceolate",
                "Eel-like"
            )
        val arrayImageEkorIkan = arrayOf(
            null,
            R.drawable.fish_tails_forked, R.drawable.fish_tails_truncate,
            R.drawable.fish_tails_rounded, R.drawable.fish_tails_lunate,
            R.drawable.fish_tails_lanceolate, R.drawable.fish_tails_eel
        )
        binding.spinnerEkor.adapter = SpinnerAdapter(
            this,
            R.layout.spinner_fish_category,
            arrayEkorIkan,
            arrayImageEkorIkan
        )

        binding.spinnerEkor.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (arrayEkorIkan[position] != "Not selected")
                    viewModel.selectedEkorIkan = arrayEkorIkan[position]
                else
                    viewModel.selectedEkorIkan = ""
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }

        val arraySiripIkan =
            arrayOf(
                "Not selected",
                "Notched (spiny)",
                "Continuous",
                "Separate dorsal fins"
            )
        val arrayImageSiripIkan = arrayOf(
            null,
            R.drawable.fish_fins_notched, R.drawable.fish_fins_continuous,
            R.drawable.fish_fins_separate
        )
        binding.spinnerSirip.adapter = SpinnerAdapter(
            this,
            R.layout.spinner_fish_category,
            arraySiripIkan,
            arrayImageSiripIkan
        )
        binding.spinnerSirip.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (arraySiripIkan[position] != "Not selected")
                    viewModel.selectedSiripIkan = arraySiripIkan[position]
                else
                    viewModel.selectedSiripIkan = ""
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }

        val arrayCorakIkan =
            arrayOf(
                "Not selected",
                "Vertical",
                "Horizontal",
                "Diagonal",
                "Chevron",
                "Spotted or false eye",
                "Saddled",
                "Banded",
                "Spotted",
                "Blotched",
                "Painted"
            )
        val arrayImageCorakIkan = arrayOf(
            null,
            R.drawable.fish_markings_vertical, R.drawable.fish_markings_horizontal,
            R.drawable.fish_markings_diagonal, R.drawable.fish_markings_chevron,
            R.drawable.fish_markings_spotted_false, R.drawable.fish_markings_saddled,
            R.drawable.fish_markings_banded, R.drawable.fish_markings_spotted,
            R.drawable.fish_markings_blotched, R.drawable.fish_markings_painted
        )
        binding.spinnerCorak.adapter = SpinnerAdapter(
            this,
            R.layout.spinner_fish_category,
            arrayCorakIkan,
            arrayImageCorakIkan
        )
        binding.spinnerCorak.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (arrayCorakIkan[position] != "Not selected")
                    viewModel.selectedCorakIkan = arrayCorakIkan[position]
                else
                    viewModel.selectedCorakIkan = ""
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }

        binding.spinnerBadan.isEnabled = viewModel.selectedFishId == "0"
        binding.spinnerMulut.isEnabled = viewModel.selectedFishId == "0"
        binding.spinnerSirip.isEnabled = viewModel.selectedFishId == "0"
        binding.spinnerEkor.isEnabled = viewModel.selectedFishId == "0"
        binding.spinnerCorak.isEnabled = viewModel.selectedFishId == "0"

        binding.spinnerBadan.setSelection(arrayBentukIkan.indexOf(viewModel.selectedBentukIkan))
        binding.spinnerMulut.setSelection(arrayMulutIkan.indexOf(viewModel.selectedMulutIkan))
        binding.spinnerSirip.setSelection(arraySiripIkan.indexOf(viewModel.selectedSiripIkan))
        binding.spinnerEkor.setSelection(arrayEkorIkan.indexOf(viewModel.selectedEkorIkan))
        binding.spinnerCorak.setSelection(arrayCorakIkan.indexOf(viewModel.selectedCorakIkan))

    }

    private fun addRadioItem(data: DetectData?) {
        val index = binding.radioGroup.size
        if (data != null) {
            val text = "${data.species.capitalize().replace("_", " ")}: ${data.probability.toInt()}"
            val radioButton = RadioButton(this)
            radioButton.id = View.generateViewId()
            radioButton.text =
                if (data.fishId == "0") "Other" else data.species.capitalize().replace("_", " ")
            radioButton.setOnClickListener {
                viewModel.selectedFish = index
                binding.tvFishName.text = data.namaInggris
                viewModel.selectedFishId = data.fishId
                viewModel.selectedBentukIkan = data.bentukTubuh
                viewModel.selectedMulutIkan = data.bentukMulut
                viewModel.selectedSiripIkan = data.bentukSirip
                viewModel.selectedEkorIkan = data.bentukEkor
                viewModel.selectedCorakIkan = data.corakTubuh
                initSpinner()

                if(data.fishId != "0"){
                    val tempBitmap = Bitmap.createBitmap(
                        binding.imgPost.width,
                        binding.imgPost.height,
                        Bitmap.Config.ARGB_8888
                    )
                    val imageBitmap = binding.imgPost.drawable?.toBitmap()!!

                    Log.d("WIDTH IMAGEVIEW", binding.imgPost.width.toString())
                    Log.d("HEIGHT IMAGEVIEW", binding.imgPost.height.toString())
                    Log.d("WIDTH ASLI", imageBitmap.width.toString())
                    Log.d("HEIGHT ASLI", imageBitmap.height.toString())

                    val tempCanvas = Canvas(tempBitmap)
                    val paint = Paint(Paint.ANTI_ALIAS_FLAG)
                    paint.style = Paint.Style.STROKE
                    paint.strokeWidth = 5f
                    paint.color = Color.RED
                    val paintText = Paint()
                    paintText.color = Color.BLACK
                    paintText.textSize = 25f
                    val textWidth = paintText.measureText(text)
                    val textHeight = paintText.textSize
                    var rectF =
                        RectF(
                            (data.x1 * binding.imgPost.width / imageBitmap.width).toFloat(),
                            (data.y1 * binding.imgPost.height / imageBitmap.height).toFloat(),
                            (data.x2 * binding.imgPost.width / imageBitmap.width).toFloat(),
                            (data.y2 * binding.imgPost.height / imageBitmap.height).toFloat()
                        )
                    tempCanvas.drawRect(rectF, paint)
                    rectF = RectF(
                        (data.x1 * binding.imgPost.width / imageBitmap.width).toFloat(),
                        (data.y1 * binding.imgPost.height / imageBitmap.height).toFloat(),
                        (data.x1 * binding.imgPost.width / imageBitmap.width).toFloat() + textWidth,
                        (data.y1 * binding.imgPost.height / imageBitmap.height).toFloat() + textHeight
                    )
                    paint.style = Paint.Style.FILL
                    paint.color = Color.LTGRAY
                    tempCanvas.drawRect(rectF, paint)
                    tempCanvas.drawText(
                        text,
                        (data.x1 * binding.imgPost.width / imageBitmap.width).toFloat(),
                        (data.y1 * binding.imgPost.height / imageBitmap.height).toFloat() + 20f,
                        paintText
                    )
                    binding.imgPostTag.setImageDrawable(
                        BitmapDrawable(
                            resources,
                            tempBitmap
                        )
                    )
                    binding.imgPostTag.visibility = View.VISIBLE
                }
                else binding.imgPostTag.setImageDrawable(null)

            }
            binding.radioGroup.addView(radioButton)
        } else {
            val radioButton = RadioButton(this)
            radioButton.id = View.generateViewId()
            radioButton.text = "Other"
            radioButton.setOnClickListener {
                binding.tvFishName.text = "Other"
                viewModel.selectedFish = index
                viewModel.selectedBentukIkan = ""
                viewModel.selectedMulutIkan = ""
                viewModel.selectedSiripIkan = ""
                viewModel.selectedEkorIkan = ""
                viewModel.selectedCorakIkan = ""
                initSpinner()
                binding.imgPostTag.visibility = View.GONE
            }
            binding.radioGroup.addView(radioButton)
        }

    }

    private fun initListener() {
        binding.etCaption.setOnFocusChangeListener { v, hasFocus ->
            viewModel.caption = binding.etCaption.text.toString()
        }
        binding.additionalTabLayout.setOnClickListener {
            viewModel.showAdditional.set(viewModel.showAdditional.get() == false)
        }

        binding.btnFind.setOnClickListener {
            if (viewModel.selectedFish != -1) {
                val i = Intent(this, MainActivity::class.java)
                i.putExtra("page", 1)
                i.putExtra("fish_id", viewModel.selectedFishId)
                i.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(i)
            } else {
                toastShort(this, "Select a fish first")
            }
        }

        binding.btnPost.setOnClickListener {
            menuSave.isVisible = true
            supportActionBar!!.title = "Post"
            binding.flipper.displayedChild = 1
        }
    }

    override fun path(path: String) {
        currentPhotoPath = path
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_save, menu)
        return true
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        menuSave = menu!!.findItem(R.id.save)
        menuSave.isVisible = false
        return super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        currentFocus?.clearFocus()
        val imgBitMap = binding.imgPost.drawable?.toBitmap()
        if (imgBitMap != null && viewModel.selectedFish != -1) {
            viewModel.postPost(
                Compressor(this).compressToFile(
                    File(
                        Utility.bitmapToFile(
                            this,
                            imgBitMap
                        ).toString()
                    )
                )
            )
        } else {
            toastShort(this, "Select a fish first")
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        try {
            if (requestCode == Constants.REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) {
                binding.imgPost.setImageBitmap(
                    BitmapFactory.decodeFile(
                        File(
                            currentPhotoPath
                        ).path
                    )
                )
                binding.imgPostPreview.setImageBitmap(
                    BitmapFactory.decodeFile(
                        File(
                            currentPhotoPath
                        ).path
                    )
                )

                viewModel.postDetect(
                    Compressor(this).compressToFile(
                        File(
                            Utility.bitmapToFile(
                                this,
                                binding.imgPost.drawable?.toBitmap()!!
                            ).toString()
                        )
                    )
                )
            } else if (resultCode == Activity.RESULT_CANCELED)
                finish()

            if (requestCode == Constants.GALLERY_IMAGE && resultCode == Activity.RESULT_OK) {
                val selectedImage: Uri = data!!.data!!
                val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)
                val cursor: Cursor? = contentResolver.query(
                    selectedImage,
                    filePathColumn, null, null, null
                )
                if (cursor == null || cursor.count < 1) {
                    Toast.makeText(this, "Gambar tidak terpilih", Toast.LENGTH_SHORT).show()
                } else {
                    cursor.moveToFirst()
                    val columnIndex: Int = cursor.getColumnIndex(filePathColumn[0])
                    if (columnIndex < 0)
                        return Toast.makeText(this, "Tidak ada gambar", Toast.LENGTH_SHORT).show()
                    val picturePath: String = cursor.getString(columnIndex)
                    cursor.close()

                    binding.imgPost.setImageBitmap(
                        BitmapFactory.decodeFile(
                            File(
                                picturePath
                            ).path
                        )
                    )
                    binding.imgPostPreview.setImageBitmap(
                        BitmapFactory.decodeFile(
                            File(
                                picturePath
                            ).path
                        )
                    )
                    val imageBitmap = binding.imgPost.drawable?.toBitmap()!!
                    Log.d("WWIDTH IMAGEVIEW", binding.imgPost.width.toString())
                    Log.d("HHEIGHT IMAGEVIEW", binding.imgPost.height.toString())
                    Log.d("WWIDTH ASLI", imageBitmap.width.toString())
                    Log.d("HHEIGHT ASLI", imageBitmap.height.toString())

                    viewModel.postDetect(
                        Compressor(this).compressToFile(
                            File(
                                Utility.bitmapToFile(
                                    this,
                                    binding.imgPost.drawable?.toBitmap()!!
                                ).toString()
                            )
                        )
                    )
                }
            } else if (resultCode == Activity.RESULT_CANCELED)
                finish()

        } catch (io: Exception) {
            io.printStackTrace()
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onBackPressed() {
        if (binding.flipper.displayedChild == 0)
            super.onBackPressed()
        else {
            menuSave.isVisible = false
            supportActionBar!!.title = "Scan"
            binding.flipper.displayedChild = 0
        }
    }

}