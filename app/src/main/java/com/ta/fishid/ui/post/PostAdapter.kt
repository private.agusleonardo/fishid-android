package com.ta.fishid.ui.post

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ta.fishid.databinding.ListBentukIkanBinding
import com.ta.fishid.model.BentukData

class PostAdapter :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var data: MutableList<BentukData> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ItemBentukHolder(
            ListBentukIkanBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is ItemBentukHolder -> holder.bind(data[position])
        }
    }

    inner class ItemBentukHolder(itemView: ListBentukIkanBinding) :
        RecyclerView.ViewHolder(itemView.root) {
        val binding = itemView
        fun bind(data: BentukData) {

//            binding.data = data
        }
    }
}

