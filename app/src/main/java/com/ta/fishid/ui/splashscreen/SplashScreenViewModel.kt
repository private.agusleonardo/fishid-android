package com.ta.fishid.ui.splashscreen

import com.ta.fishid.cache.PreferencesHelper
import com.ta.fishid.ui.base.BaseViewModel
import javax.inject.Inject

class SplashScreenViewModel @Inject constructor(
    val preferencesHelper: PreferencesHelper
) : BaseViewModel()