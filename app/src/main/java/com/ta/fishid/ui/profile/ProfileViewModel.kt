package com.ta.fishid.ui.profile

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.ta.fishid.cache.PreferencesHelper
import com.ta.fishid.repository.UserRepository
import com.ta.fishid.ui.base.BaseViewModel
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import javax.inject.Inject

class ProfileViewModel @Inject constructor(
    val userRepository: UserRepository,
    val preferencesHelper: PreferencesHelper
) : BaseViewModel() {
    var showLoading = ObservableBoolean(false)
    var showEmpty = ObservableBoolean(false)
    var showError = ObservableBoolean(false)
    var isLoading = ObservableBoolean(false)

    var changeFragment = ObservableBoolean(false)
    var page = ObservableInt(0)
    var perPage = ObservableInt(20)
    var offset = ObservableInt(0)
    var userId = ObservableField("")
    var username = ObservableField("")
    var bio = ObservableField("")
    var picture = ObservableField("")
    var oldPassword = ObservableField("")
    var newPassword = ObservableField("")

    var checkGetProfileTrigger: MutableLiveData<Boolean> = MutableLiveData()
    var checkPostProfileTrigger: MutableLiveData<Boolean> = MutableLiveData()
    var checkPasswordTrigger: MutableLiveData<Boolean> = MutableLiveData()


    private var body: MultipartBody.Part? = null


    var checkGetProfile = Transformations.switchMap(checkGetProfileTrigger) {
        userRepository.getProfile(
            userId.get() ?: "", page.get(), perPage.get(), offset.get()
        )
    }

    fun getProfile() {
        checkGetProfileTrigger.value = checkGetProfileTrigger.value == false
    }

    var checkPostProfile = Transformations.switchMap(checkPostProfileTrigger) {
        if (body != null)
            userRepository.postProfile(
                preferencesHelper.getAccountRx().blockingGet().userId!!,
                username.get() ?: "",
                bio.get() ?: "",
                body!!
            )
        else
            userRepository.postProfile(
                preferencesHelper.getAccountRx().blockingGet().userId!!,
                username.get() ?: "",
                bio.get() ?: ""
            )
    }

    fun postEditProfile(file: File) {
        val requestFile = RequestBody.create("image/*".toMediaTypeOrNull(), file)

        body = MultipartBody.Part.createFormData("profile", file.name, requestFile)
        checkPostProfileTrigger.value = checkPostProfileTrigger.value == false
    }

    fun postProfile() {
        checkPostProfileTrigger.value = checkPostProfileTrigger.value == false
    }

    var checkPassword = Transformations.switchMap(checkPasswordTrigger) {
        userRepository.postPassword(
            preferencesHelper.getAccountRx().blockingGet().userId!!,
            oldPassword.get() ?: "",
            newPassword.get() ?: ""
        )
    }

    fun postPassword() {
        checkPasswordTrigger.value = checkPasswordTrigger.value == false
    }


    fun onRefresh() {
        setRefreshing(true)
        offset.set(0)
        page.set(0)
        getProfile()
    }

    fun setRefreshing(action: Boolean) {
        isLoading.set(action)
    }

    fun setShowLoading(action: Boolean) {
        showLoading.set(action)
    }

    fun setShowError(action: Boolean) {
        showError.set(action)
    }

    fun setShowEmpty(action: Boolean) {
        showEmpty.set(action)
    }
}