package com.ta.fishid.ui.comment

import android.text.SpannableStringBuilder
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.text.bold
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.ta.fishid.Constants
import com.ta.fishid.R
import com.ta.fishid.databinding.ListCommentBinding
import com.ta.fishid.model.Comments
import com.ta.fishid.util.Utility

class CommentAdapter(val context: CommentActivity) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var data: MutableList<Comments> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ItemCommentHolder(
            ListCommentBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is ItemCommentHolder -> holder.bind(data[position])
        }
    }

    inner class ItemCommentHolder(itemView: ListCommentBinding) :
        RecyclerView.ViewHolder(itemView.root) {
        val binding = itemView
        fun bind(data: Comments) {
            binding.tvCaption.text = SpannableStringBuilder()
                .bold { append(data.username) }
                .append(" ${data.comment}")

            Glide.with(context)
                .load(Constants.BaseURL + data.profilePict)
                .error(R.drawable.ic_account)
                .into(binding.circleImageView)

            binding.tvTime.text = Utility.getDifferenceTime(Utility.convertStringToDate(3,data.waktu).time)
            binding.tvCaption.setOnClickListener {
                context.profile(data)
            }
        }
    }
}