package com.ta.fishid.ui.postDetail

import android.app.Activity
import android.content.Intent
import android.graphics.*
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.util.Log
import android.view.View
import androidx.core.graphics.drawable.toDrawable
import androidx.core.text.bold
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.target.Target
import com.bumptech.glide.request.transition.Transition
import com.ta.fishid.Constants
import com.ta.fishid.R
import com.ta.fishid.api.model.Status
import com.ta.fishid.databinding.ActivityPostDetailBinding
import com.ta.fishid.databinding.ListHomePostBinding
import com.ta.fishid.model.PostData
import com.ta.fishid.ui.base.BaseActivity
import com.ta.fishid.ui.comment.CommentActivity
import com.ta.fishid.ui.profile.ProfileActivity
import com.ta.fishid.util.Utility
import com.ta.fishid.util.Utility.toastShort
import kotlinx.android.synthetic.main.dialog_info.*
import kotlinx.android.synthetic.main.dialog_report.*
import org.json.JSONObject
import javax.inject.Inject


class PostDetailActivity : BaseActivity() {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var viewModel: PostDetailViewModel
    private lateinit var binding: ActivityPostDetailBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_post_detail)
        viewModel = initViewModel(PostDetailViewModel::class.java, viewModelFactory)

        setToolbar("", true)
        initObserver()

        viewModel.postId.set(intent.getStringExtra("post_id"))
        viewModel.getPost()

    }

    private fun setInfoDialog(data: PostData) {
        val dialog = Utility.showInfoDialog(this)

        binding.imgInfo.setOnClickListener {
            dialog.show()
            dialog.tv_body.text = data.bentukTubuh
            dialog.tv_mouth.text = data.bentukMulut
            dialog.tv_tail.text = data.bentukEkor
            dialog.tv_fin.text = data.bentukSirip
            dialog.tv_pattern.text = data.corakTubuh

            if (Constants.arrayBentukIkan.indexOf(data.bentukTubuh) != -1)
                dialog.img_body.setImageResource(
                    Constants.arrayImageBentukIkan[Constants.arrayBentukIkan.indexOf(
                        data.bentukTubuh
                    )]!!
                )
            else dialog.img_body.setImageResource(R.drawable.fish_unidentified)

            if (Constants.arrayMulutIkan.indexOf(data.bentukMulut) != -1)
                dialog.img_mouth.setImageResource(
                    Constants.arrayImageMulutIkan[Constants.arrayMulutIkan.indexOf(
                        data.bentukMulut
                    )]!!
                )
            else dialog.img_mouth.setImageResource(R.drawable.fish_unidentified)

            if (Constants.arrayEkorIkan.indexOf(data.bentukEkor) != -1)
                dialog.img_tail.setImageResource(
                    Constants.arrayImageEkorIkan[Constants.arrayEkorIkan.indexOf(
                        data.bentukEkor
                    )]!!
                )
            else dialog.img_tail.setImageResource(R.drawable.fish_unidentified)

            if (Constants.arraySiripIkan.indexOf(data.bentukSirip) != -1)
                dialog.img_fin.setImageResource(
                    Constants.arrayImageSiripIkan[Constants.arraySiripIkan.indexOf(
                        data.bentukSirip
                    )]!!
                )
            else dialog.img_fin.setImageResource(R.drawable.fish_unidentified)

            if (Constants.arrayCorakIkan.indexOf(data.corakTubuh) != -1)
                dialog.img_pattern.setImageResource(
                    Constants.arrayImageCorakIkan[Constants.arrayCorakIkan.indexOf(
                        data.corakTubuh
                    )]!!
                )
            else dialog.img_pattern.setImageResource(R.drawable.fish_unidentified)
        }
    }

    private fun setScanTag(data: PostData) {
        var intrinsicWidth: Int? = 0
        var intrinsicHeight: Int? = 0

        Glide.with(this)
            .asBitmap()
            .load(Constants.BaseURL + data.image)
            .listener(object : RequestListener<Bitmap> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<Bitmap>?,
                    isFirstResource: Boolean
                ): Boolean {
                    return false
                }

                override fun onResourceReady(
                    resource: Bitmap?,
                    model: Any?,
                    target: Target<Bitmap>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    val imageView = binding.imgPost
                    val width = imageView.measuredWidth
                    val height =
                        width * resource!!.toDrawable(resources).intrinsicHeight / resource.toDrawable(
                            resources
                        ).intrinsicWidth
                    if (imageView.layoutParams.height != height) {
                        imageView.layoutParams.height = height
                        imageView.requestLayout()
                    }
                    return false
                }
            })
            .into(object : CustomTarget<Bitmap>() {
                override fun onResourceReady(
                    resource: Bitmap,
                    transition: Transition<in Bitmap>?
                ) {
                    binding.imgPost.setImageBitmap(resource)
                    intrinsicWidth = binding.imgPost.drawable?.intrinsicWidth
                    intrinsicHeight = binding.imgPost.drawable?.intrinsicHeight
                }

                override fun onLoadCleared(placeholder: Drawable?) {}
            })

        binding.imgPostTag.visibility = View.GONE

        if (data.species != "Other") {
            binding.imgScan.visibility = View.VISIBLE
            binding.imgScan.setOnClickListener {
                if (binding.imgPostTag.visibility == View.VISIBLE) {
                    binding.imgPostTag.visibility = View.GONE
                } else {

                    val tempBitmap = Bitmap.createBitmap(
                        binding.imgPost.width,
                        binding.imgPost.height,
                        Bitmap.Config.ARGB_8888
                    )

                    val text = "${data.species!!.capitalize().replace(
                        "_",
                        " "
                    )}: ${data.probability.toInt()}"


                    val tempCanvas = Canvas(tempBitmap)
                    val paint = Paint(Paint.ANTI_ALIAS_FLAG)
                    paint.style = Paint.Style.STROKE
                    paint.strokeWidth = 5f
                    paint.color = Color.RED
                    val paintText = Paint()
                    paintText.color = Color.BLACK
                    paintText.textSize = 25f
                    val textWidth = paintText.measureText(text)
                    val textHeight = paintText.textSize
                    var rectF =
                        RectF(
                            (data.x1 * binding.imgPost.width / intrinsicWidth!!).toFloat(),
                            (data.y1 * binding.imgPost.height / intrinsicHeight!!).toFloat(),
                            (data.x2 * binding.imgPost.width / intrinsicWidth!!).toFloat(),
                            (data.y2 * binding.imgPost.height / intrinsicHeight!!).toFloat()
                        )
                    tempCanvas.drawRect(rectF, paint)
                    rectF = RectF(
                        (data.x1 * binding.imgPost.width / intrinsicWidth!!).toFloat(),
                        (data.y1 * binding.imgPost.height / intrinsicHeight!!).toFloat(),
                        (data.x1 * binding.imgPost.width / intrinsicWidth!!).toFloat() + textWidth,
                        (data.y1 * binding.imgPost.height / intrinsicHeight!!).toFloat() + textHeight
                    )
                    paint.style = Paint.Style.FILL
                    paint.color = Color.LTGRAY
                    tempCanvas.drawRect(rectF, paint)
                    tempCanvas.drawText(
                        text,
                        (data.x1 * binding.imgPost.width / intrinsicWidth!!).toFloat(),
                        (data.y1 * binding.imgPost.height / intrinsicHeight!!).toFloat() + 20f,
                        paintText
                    )
                    binding.imgPostTag.setImageDrawable(
                        BitmapDrawable(
                            resources,
                            tempBitmap
                        )
                    )
                    binding.imgPostTag.visibility = View.VISIBLE
                }
            }
        } else binding.imgScan.visibility = View.GONE
    }

    private fun initObserver() {
        viewModel.checkPost.observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    val data = it.data
                    if (data != null) {
                        setToolbar(data.username, true)
                        setInfoDialog(data)
                        setScanTag(data)
                        binding.tvUsername.text = data.username
                        binding.tvLike.text = "likes ${data.likeCount}"
                        if (data.likeStatus) binding.btLike.setImageResource(R.drawable.ic_love_filled)

                        if (data.commentCount > 0 && data.commentStatus)
                            binding.tvComment.text = "View all ${data.commentCount} comments"
                        else binding.tvComment.visibility = View.GONE

                        binding.tvComment.setOnClickListener {
                            comment(data)
                        }

                        binding.tvUsername.setOnClickListener {
                            profile(data)
                        }

                        binding.tvDate.text =
                            Utility.getHomeDifferenceTime(
                                Utility.convertStringToDate(
                                    3,
                                    data.tanggal
                                ).time
                            )

                        binding.tvCaption.text = SpannableStringBuilder()
                            .bold { append(data.username) }
                            .append(" ${data.caption}")

                        binding.btLike.setOnClickListener {
                            if (!data.likeStatus) {
                                data.likeStatus = true
                                likePost(data.postId.toString())
                                binding.tvLike.text = "likes ${++data.likeCount}"
                                binding.btLike.setImageResource(R.drawable.ic_love_filled)

                            } else {
                                data.likeStatus = false
                                unlikePost(data.postId.toString())
                                binding.tvLike.text = "likes ${--data.likeCount}"
                                binding.btLike.setImageResource(R.drawable.ic_love_outlined)
                            }
                        }

                        binding.btComment.setOnClickListener {
                            comment(data)
                        }

                        if (data.userId.toString() == viewModel.preferencesHelper.getAccountRx().blockingGet().userId) {
                            binding.tvReport.visibility = View.GONE
                            binding.tvDelete.visibility = View.VISIBLE
                            binding.tvDelete.setOnClickListener {
                                Utility.showCustomPromptDialog(this, "Delete Post?",
                                    {
                                        it.dismiss()
                                        delete(data.postId)
                                    }, {
                                        it.dismiss()
                                    }
                                )
                            }
                        } else {
                            binding.tvReport.visibility = View.VISIBLE
                            binding.tvDelete.visibility = View.GONE
                            binding.tvReport.setOnClickListener {
                                Utility.showReportDialog(this) {
                                    it.dismiss()
                                    when {
                                        it.layout_wrong_info.tag == "1" -> report(
                                            data.postId,
                                            "Wrong Information"
                                        )
                                        it.layout_no_fish.tag == "1" -> report(
                                            data.postId,
                                            "No Fish Related"
                                        )
                                        it.layout_other.tag == "1" -> report(data.postId, "Other")
                                        else -> toastShort(this, "No Option Selected")
                                    }
                                }
                            }
                        }

                        Glide.with(this)
                            .load(Constants.BaseURL + data.profilePict)
                            .error(R.drawable.ic_account)
                            .into(binding.circleImageView)

                        Glide.with(this)
                            .load(Constants.BaseURL + data.image)
                            .error(R.drawable.bg_rectangle_grey)
                            .into(binding.imgPost)

                    }
                }
            }
        })

        viewModel.checkLikePost.observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    Log.d("LIKE", "LIKED")
                }
            }
        })

        viewModel.checkUnlikePost.observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    Log.d("UNLIKE", "UNLIKED")
                }
            }
        })

        viewModel.checkReport.observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    toastShort(this, "Report Submitted")
                }
                Status.ERROR -> {
                    toastShort(this, "Report already submitted")
                }
            }
        })

        viewModel.checkDelete.observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    val returnIntent = Intent()
                    returnIntent.putExtra("result", "delete")
                    setResult(Activity.RESULT_OK, returnIntent)
                    toastShort(this, "Delete Success")
                    finish()
                }
            }
        })
    }

    fun likePost(postId: String) {
        viewModel.likePost(postId)
    }

    fun unlikePost(postId: String) {
        viewModel.unlikePost(postId)
    }

    fun profile(data: PostData) {
        startActivity(
            Intent(this, ProfileActivity::class.java)
                .putExtra("user_id", data.userId.toString())
                .putExtra("username", data.username)
        )
    }

    fun comment(data: PostData) {
        startActivity(
            Intent(this, CommentActivity::class.java)
                .putExtra("post_id", data.postId)
                .putExtra("caption", data.caption)
                .putExtra("username", data.username)
                .putExtra("profile_pict", data.profilePict)
        )
    }

    fun report(postId: Int, reason: String) {
        viewModel.report(postId.toString(), reason)
    }

    fun delete(postId: Int) {
        viewModel.delete(postId.toString())
    }
}