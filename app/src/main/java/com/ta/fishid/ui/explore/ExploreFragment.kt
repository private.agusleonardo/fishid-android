package com.ta.fishid.ui.explore

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.bumptech.glide.Glide
import com.ta.fishid.Constants
import com.ta.fishid.api.model.Status
import com.ta.fishid.databinding.FragmentExploreBinding
import com.ta.fishid.model.FilterData
import com.ta.fishid.model.PostData
import com.ta.fishid.ui.main.MainActivity
import com.ta.fishid.ui.postDetail.PostDetailActivity
import com.ta.fishid.util.OnLoadMoreListener
import com.ta.fishid.util.Utility
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class ExploreFragment(val activity: MainActivity) : DaggerFragment() {

    @Inject
    lateinit var viewmodelFactory: ViewModelProvider.Factory
    private lateinit var viewModel: ExploreViewModel
    private lateinit var binding: FragmentExploreBinding

    lateinit var adapterExplore: ExploreAdapter
    lateinit var adapterFilter: ExploreAdapter
    lateinit var adapterDescription: ExploreAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProviders.of(this, viewmodelFactory).get(ExploreViewModel::class.java)
        binding = FragmentExploreBinding.inflate(inflater, container, false)
        binding.viewModel = viewModel

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initAdapter()
        initObserver()
        initListener()

        binding.swipeRefreshLayout2.visibility = View.GONE

        viewModel.getFish()
        if (activity.fishId.isNotEmpty()) {
            viewModel.filter.set(false)
            viewModel.fishId.set(activity.fishId)
            viewModel.getExplore()
            binding.layoutReadmore.visibility =
                if (activity.fishId != "0") View.VISIBLE else View.GONE
            binding.swipeRefreshLayout2.visibility = View.VISIBLE
            binding.layoutExplore.visibility = View.GONE
        }

    }

    private fun initListener() {
        binding.imgFilter.setOnClickListener {
            viewModel.filter.set(!viewModel.filter.get())
            when (viewModel.filter.get()) {
                true -> {
                    binding.layoutReadmore.visibility = View.GONE
                    binding.layoutExplore.visibility = View.GONE
                }
                false -> {
                    if (viewModel.fishId.get()!!.isNotEmpty() && viewModel.fishId.get() != "0") {
                        binding.layoutReadmore.visibility = View.VISIBLE
                        binding.layoutExplore.visibility = View.GONE
                    } else if (viewModel.fishId.get()!!.isNotEmpty())
                        binding.layoutExplore.visibility = View.GONE
                    else
                        binding.layoutExplore.visibility = View.VISIBLE
                }
            }
        }

        binding.constraintLayout.setOnClickListener {
            viewModel.filter.set(!viewModel.filter.get())
            when (viewModel.filter.get()) {
                true -> {
                    binding.layoutReadmore.visibility = View.GONE
                    binding.layoutExplore.visibility = View.GONE
                }
                false -> {
                    if (viewModel.fishId.get()!!.isNotEmpty() && viewModel.fishId.get() != "0") {
                        binding.layoutReadmore.visibility = View.VISIBLE
                        binding.layoutExplore.visibility = View.GONE
                    } else if (viewModel.fishId.get()!!.isNotEmpty())
                        binding.layoutExplore.visibility = View.GONE
                    else
                        binding.layoutExplore.visibility = View.VISIBLE
                }
            }
        }

        binding.imgBack.setOnClickListener {
            binding.flipper.displayedChild = 0
        }

        binding.layoutReadmore.setOnClickListener {
            binding.flipper.displayedChild = 1
        }

        adapterExplore.setOnloadMoreListener(object : OnLoadMoreListener {
            override fun onLoadMore() {
                viewModel.page.set(viewModel.page.get() + 1)
                viewModel.getExplore()
            }
        })
    }

    private fun initAdapter() {
        binding.recyclerViewFilter.layoutManager = GridLayoutManager(activity, 2)
        adapterFilter = ExploreAdapter(this, "Filter", null)
        binding.recyclerViewFilter.adapter = adapterFilter

        binding.recyclerViewExplore.layoutManager = GridLayoutManager(activity, 3)
        adapterExplore = ExploreAdapter(this, "Explore", binding.recyclerViewExplore)
        binding.recyclerViewExplore.adapter = adapterExplore

        binding.recyclerViewDescription.layoutManager = GridLayoutManager(activity, 3)
        adapterDescription = ExploreAdapter(this, "Description", null)
        binding.recyclerViewDescription.adapter = adapterDescription

    }

    private fun initObserver() {
        viewModel.checkFilter.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    if (it.data != null) {
                        adapterFilter.dataFilter.clear()
                        adapterFilter.dataFilter.addAll(it.data)
                        adapterFilter.notifyDataSetChanged()
                    }
                }
            }
        })

        viewModel.checkExplore.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    if (it.data != null) {
                        binding.tvFilterFish.text =
                            if (it.data.fish.namaLatin != "Other") "${it.data.fish.namaInggris} (${it.data.fish.namaLatin})"
                            else "Other"
                        Glide.with(this).asBitmap()
                            .load(Constants.BaseURL + it.data.fish.fishPicture)
                            .into(binding.imgReadmore)
                        Glide.with(this).asBitmap()
                            .load(Constants.BaseURL + it.data.fish.fishPicture)
                            .into(binding.imgDescription)
                        binding.tvDescription.text = it.data.fish.deskripsi.replace("\\n", "\n")
                        binding.tvReadmore.text = it.data.fish.deskripsi.replace("\\n", "\n")
                        if (viewModel.page.get() == 0) {
                            adapterExplore.isComplete = false
                            adapterExplore.dataExplore.clear()

                            adapterDescription.dataExplore.clear()
                            if (it.data.post.size > 3)
                                for (i in 0..2)
                                    adapterDescription.dataExplore.add(it.data.post[i])
                            else
                                for (element in it.data.post)
                                    adapterDescription.dataExplore.add(element)
                            adapterDescription.notifyDataSetChanged()
                        }
                        if (it.data.post.size < viewModel.perPage.get()) adapterExplore.isComplete =
                            true
                        adapterExplore.dataExplore.addAll(it.data.post)
                        adapterExplore.notifyDataSetChanged()
                        viewModel.setRefreshing(false)
                        adapterExplore.isLoading = false
                    }
                }
                Status.LOADING -> {
                    adapterExplore.isLoading = true
                }
                Status.ERROR -> {
                    Utility.toastShort(requireContext(), "Something went wrong")
                    viewModel.setRefreshing(false)
                }
            }
        })
    }

    fun selectFilter(data: FilterData) {
        if (data.fishId != "0") {
            binding.layoutReadmore.visibility = View.VISIBLE
            binding.tvFilterFish.text = "${data.namaInggris} (${data.namaLatin})"
        } else {
            binding.layoutReadmore.visibility = View.GONE
            binding.tvFilterFish.text = data.namaInggris
        }
        viewModel.filter.set(false)
        viewModel.fishId.set(data.fishId)
        viewModel.getExplore()
    }

    fun intentPostDetail(postId: String) {
        startActivity(
            Intent(
                activity,
                PostDetailActivity::class.java
            ).putExtra("post_id", postId).putExtra(
                "username",
                viewModel.preferencesHelper.getAccountRx().blockingGet().username
            )
        )
    }

}