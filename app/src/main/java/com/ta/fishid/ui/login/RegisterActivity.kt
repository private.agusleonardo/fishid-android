package com.ta.fishid.ui.login

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.ta.fishid.R
import com.ta.fishid.api.model.Status
import com.ta.fishid.databinding.ActivityRegisterBinding
import com.ta.fishid.ui.base.BaseActivity
import com.ta.fishid.ui.main.MainActivity
import org.json.JSONObject
import javax.inject.Inject

class RegisterActivity : BaseActivity() {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var viewModel: LoginViewModel

    private lateinit var binding: ActivityRegisterBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_register)
        viewModel = initViewModel(LoginViewModel::class.java, viewModelFactory)
        binding.viewModel = viewModel

        setLoadingDialog(this)
        initObserver()
        initListener()
    }

    private fun initListener() {
        binding.btRegister.setOnClickListener {
            registerCheck()
        }
        binding.tvLogin.setOnClickListener {
            startActivity(Intent(this, RegisterActivity::class.java))
            finish()
        }
    }

    private fun registerCheck() {
        if (!binding.etUsername.text.isNullOrEmpty() || !binding.etPass.text.isNullOrEmpty() || !binding.etEmail.text.isNullOrEmpty()) {
            viewModel.username.set(binding.etUsername.text.toString())
            viewModel.bio.set("")
//            viewModel.email.set(binding.etEmail.text.toString())
            if (binding.etUsername.text.matches(Regex("^[a-zA-Z0-9._-]{3,}\$")))
                if (binding.etUsername.text.count() < 6 || binding.etUsername.text.count() > 20)
                    showMessage("Your username must be between 6 and 20 characters")
                else if (binding.etPass.text.count() < 8 || binding.etPass.text.count() > 16)
                    showMessage("Your password must be between 8 and 16 characters")
                else
                    if (binding.etUsername.text.contains(" "))
                        showMessage("Username must not contains whitespace")
                    else
                        if (binding.etPass.text.toString() == binding.etEmail.text.toString()) {
                            viewModel.password.set(binding.etPass.text.toString())
                            binding.tvMessage.visibility = View.GONE
                            viewModel.register()
                        } else showMessage("Password and confirm password must be the same")
            else showMessage("Username must not contains special characters or whitespace")
        } else showMessage("Form must not empty")
    }

    private fun showMessage(text: String) {
        binding.tvMessage.visibility = View.VISIBLE
        binding.tvMessage.text = text
    }

    private fun initObserver() {
        viewModel.registerCheck.observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    if (it.data != null) {
                        dismissDialog()
                        startActivity(Intent(this, MainActivity::class.java))
                        finish()
                    }
                }
                Status.VALIDATION_ERROR -> {
                    val json = JSONObject(it.message!!)
                    dismissDialog()
                    showMessage(json.getString("message"))
                }
                Status.ERROR -> {
                    dismissDialog()
                    Toast.makeText(this, "Server Error", Toast.LENGTH_SHORT).show()
                }
                Status.LOADING -> showDialog()
            }
        })
    }
}