package com.ta.fishid.ui.home

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.ta.fishid.api.model.BaseResponse
import com.ta.fishid.api.model.Resource
import com.ta.fishid.api.model.Status
import com.ta.fishid.databinding.FragmentHomeBinding
import com.ta.fishid.model.PostData
import com.ta.fishid.ui.comment.CommentActivity
import com.ta.fishid.ui.profile.ProfileActivity
import com.ta.fishid.util.OnLoadMoreListener
import com.ta.fishid.util.Utility.toastShort
import dagger.android.support.DaggerFragment
import org.json.JSONObject
import javax.inject.Inject

class HomeFragment : DaggerFragment() {

    @Inject
    lateinit var viewmodelFactory: ViewModelProvider.Factory
    lateinit var viewModel: HomeViewModel
    private lateinit var binding: FragmentHomeBinding

    lateinit var adapter: HomeAdapter
    var position: Int = -1

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProviders.of(this, viewmodelFactory).get(HomeViewModel::class.java)
        binding = FragmentHomeBinding.inflate(inflater, container, false)
        binding.viewModel = viewModel

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initAdapter()
        initObserver()
        initListener()
    }

    private fun initListener() {
        adapter.setOnloadMoreListener(object : OnLoadMoreListener {
            override fun onLoadMore() {
                viewModel.page.set(viewModel.page.get() + 1)
                viewModel.getHome()
            }
        })
    }

    private fun initAdapter() {
        adapter = HomeAdapter(this, binding.recyclerView)
        binding.recyclerView.adapter = adapter
    }

    private fun initObserver() {
        viewModel.checkHome.observe(viewLifecycleOwner, Observer {
            if (!viewModel.changeFragment.get()) {
                when (it.status) {
                    Status.SUCCESS -> {
                        if (it.data != null) {
                            if (viewModel.page.get() == 0) {
                                adapter.isComplete = false
                                adapter.data.clear()
                            }
                            if (it.data.size < viewModel.perPage.get()) adapter.isComplete = true
                            adapter.data.addAll(it.data)
                            adapter.notifyDataSetChanged()
                            viewModel.setRefreshing(false)

                            adapter.isLoading = false
                        }

                    }
                    Status.LOADING -> {
                        adapter.isLoading = true
                    }
                    Status.ERROR -> {
                        toastShort(requireContext(), "Something went wrong")
                        viewModel.setRefreshing(false)
                    }
                }
            }
        })

        viewModel.checkLikePost.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    Log.d("LIKE", "LIKED")
                }
            }
        })

        viewModel.checkUnlikePost.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    Log.d("UNLIKE", "UNLIKED")
                }
            }
        })

        viewModel.checkReport.observe(viewLifecycleOwner, Observer {
            if (!viewModel.changeFragment.get()) {
                when (it.status) {
                    Status.SUCCESS -> {
                        viewModel.offset.set(viewModel.offset.get() + 1)
                        adapter.data.removeAt(position)
                        adapter.notifyItemRemoved(position)
                        toastShort(requireContext(), "Report submitted")

                    }
                    Status.ERROR -> {
                        toastShort(requireContext(), "Report already submitted")
                    }
                }
            }
        })

        viewModel.checkDelete.observe(viewLifecycleOwner, Observer {
            if (!viewModel.changeFragment.get()) {
                when (it.status) {
                    Status.SUCCESS -> {
                        viewModel.offset.set(viewModel.offset.get() + 1)
                        adapter.data.removeAt(position)
                        adapter.notifyItemRemoved(position)
                        toastShort(requireContext(), "Delete Success")
                    }
                }
            }
        })
    }

    fun likePost(postId: String) {
        viewModel.likePost(postId)
    }

    fun unlikePost(postId: String) {
        viewModel.unlikePost(postId)
    }

    fun profile(data: PostData) {
        startActivity(
            Intent(context, ProfileActivity::class.java)
                .putExtra("user_id", data.userId.toString())
                .putExtra("username", data.username)
        )
    }

    fun comment(data: PostData) {
        startActivity(
            Intent(context, CommentActivity::class.java)
                .putExtra("post_id", data.postId)
                .putExtra("caption", data.caption)
                .putExtra("username", data.username)
                .putExtra("profile_pict", data.profilePict)
        )
    }

    fun report(postId: Int, reason: String, position: Int) {
        this.position = position
        viewModel.report(postId.toString(), reason)
    }

    fun delete(postId: Int, position: Int) {
        this.position = position
        viewModel.delete(postId.toString())
    }

    override fun onResume() {
        super.onResume()
        if (viewModel.page.get() == 0) viewModel.getHome()
        viewModel.changeFragment.set(false)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        viewModel.page.set(0)
        viewModel.offset.set(0)
        viewModel.changeFragment.set(true)
    }
}