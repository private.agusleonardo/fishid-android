package com.ta.fishid.ui.profile

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.ta.fishid.Constants
import com.ta.fishid.api.model.Status
import com.ta.fishid.databinding.FragmentProfileBinding
import com.ta.fishid.model.PostData
import com.ta.fishid.ui.postDetail.PostDetailActivity
import com.ta.fishid.util.OnLoadMoreListener
import com.ta.fishid.util.Utility
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class ProfileFragment : DaggerFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var viewModel: ProfileViewModel
    private lateinit var binding: FragmentProfileBinding
    lateinit var adapter: ProfileAdapter
    var position = -1

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(ProfileViewModel::class.java)
        binding = FragmentProfileBinding.inflate(inflater, container, false)
        binding.viewModel = viewModel

        viewModel.username.set(viewModel.preferencesHelper.getAccountRx().blockingGet().username)
        viewModel.bio.set(viewModel.preferencesHelper.getAccountRx().blockingGet().bio)
        viewModel.picture.set(Constants.BaseURL + viewModel.preferencesHelper.getAccountRx().blockingGet().profilePict)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initObserver()
        initAdapter()
        initListener()

        viewModel.userId.set(viewModel.preferencesHelper.getAccountRx().blockingGet().userId)
        viewModel.getProfile()
    }

    private fun initAdapter() {
        binding.recyclerView.layoutManager = GridLayoutManager(activity, 3)
        adapter = ProfileAdapter(this, null, binding.recyclerView)
        binding.recyclerView.adapter = adapter
    }

    private fun initObserver() {
        viewModel.checkGetProfile.observe(viewLifecycleOwner, Observer {
            if (!viewModel.changeFragment.get()) {
                when (it.status) {
                    Status.SUCCESS -> {
                        if (it.data != null) {
                            if (viewModel.page.get() == 0) {
                                adapter.isComplete = false
                                adapter.data.clear()
                                adapter.notifyDataSetChanged()
                            }
                            if (it.data.post.size < viewModel.perPage.get()) adapter.isComplete =
                                true
                            adapter.data.addAll(it.data.post)
                            adapter.notifyDataSetChanged()
                            viewModel.setRefreshing(false)

                            adapter.isLoading = false
                        }
                    }
                    Status.LOADING -> {
                        adapter.isLoading = true
                    }
                    Status.ERROR -> {
                        Utility.toastShort(requireContext(), "Something went wrong")
                        viewModel.setRefreshing(false)
                    }
                }
            }
        })
    }

    private fun initListener() {
        binding.btEdit.setOnClickListener {
            startActivity(Intent(activity, EditProfileActivity::class.java))
        }

        adapter.setOnloadMoreListener(object : OnLoadMoreListener {
            override fun onLoadMore() {
                viewModel.page.set(viewModel.page.get() + 1)
                viewModel.getProfile()
            }
        })
    }

    override fun onResume() {
        super.onResume()
        viewModel.username.set(viewModel.preferencesHelper.getAccountRx().blockingGet().username)
        viewModel.bio.set(viewModel.preferencesHelper.getAccountRx().blockingGet().bio)
        viewModel.picture.set(Constants.BaseURL + viewModel.preferencesHelper.getAccountRx().blockingGet().profilePict)
        if (viewModel.page.get() == 0) viewModel.getProfile()
        viewModel.changeFragment.set(false)
    }

    fun intentPostDetail(data: PostData, position: Int) {
        this.position = position
        startActivityForResult(
            Intent(
                activity,
                PostDetailActivity::class.java
            ).putExtra("post_id", data.postId.toString()).putExtra(
                "username",
                data.username
            ),
            1
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
            viewModel.offset.set(viewModel.offset.get() + 1)
            adapter.data.removeAt(position)
            adapter.notifyItemRemoved(position)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        viewModel.page.set(0)
        viewModel.offset.set(0)
        viewModel.changeFragment.set(true)
    }

}