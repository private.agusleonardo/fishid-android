package com.ta.fishid.ui.collection

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.ta.fishid.Constants
import com.ta.fishid.api.model.Status
import com.ta.fishid.databinding.FragmentCollectionBinding
import com.ta.fishid.model.PostData
import com.ta.fishid.ui.postDetail.PostDetailActivity
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class CollectionFragment : DaggerFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var viewModel: CollectionViewModel
    private lateinit var binding: FragmentCollectionBinding
    lateinit var adapterLike: CollectionAdapter
    lateinit var adapterHistory: CollectionAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel =
            ViewModelProviders.of(this, viewModelFactory).get(CollectionViewModel::class.java)
        binding = FragmentCollectionBinding.inflate(inflater, container, false)
        binding.viewModel = viewModel

        viewModel.username.set(viewModel.preferencesHelper.getAccountRx().blockingGet().username)
        viewModel.picture.set(Constants.BaseURL + viewModel.preferencesHelper.getAccountRx().blockingGet().profilePict)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initObserver()
        initListener()
        initAdapter()
        binding.radioLike.isChecked = true
    }

    private fun initAdapter() {
        adapterLike = CollectionAdapter(this, Constants.Like)
        binding.recyclerViewLike.adapter = adapterLike
        binding.recyclerViewLike.layoutManager = GridLayoutManager(activity, 3)

        adapterHistory = CollectionAdapter(this, Constants.History)
        binding.recyclerViewHistory.adapter = adapterHistory
    }

    private fun initObserver() {
        viewModel.checkGetLike.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    if (it.data != null) {
                        adapterLike.dataLike.clear()
                        adapterLike.dataLike.addAll(it.data.post)
                        adapterLike.notifyDataSetChanged()
                        viewModel.setRefreshing(false)
                        if (adapterLike.dataLike.isEmpty())
                            binding.layoutLikeEmpty.visibility = View.VISIBLE
                        else binding.layoutLikeEmpty.visibility = View.GONE
                    }
                }
            }
        })
        viewModel.checkGetHistory.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    if (it.data != null) {
                        adapterHistory.dataHistory.clear()
                        adapterHistory.dataHistory.addAll(it.data)
                        adapterHistory.notifyDataSetChanged()
                        if (adapterHistory.dataHistory.isEmpty())
                            binding.layoutHistoryEmpty.visibility = View.VISIBLE
                        else binding.layoutHistoryEmpty.visibility = View.GONE
                    }
                }
            }
        })
    }

    private fun initListener() {
        binding.radioLike.setOnClickListener {
            binding.flipper.displayedChild = 0
        }
        binding.radioHistory.setOnClickListener {
            binding.flipper.displayedChild = 1
        }
    }

    override fun onResume() {
        super.onResume()
        viewModel.getCollectionLike()
        viewModel.getCollectionHistory()
    }

    fun intentPostDetail(data: PostData) {
        startActivity(
            Intent(
                activity,
                PostDetailActivity::class.java
            ).putExtra("post_id", data.postId.toString()).putExtra(
                "username",
                data.username
            )
        )
    }

}