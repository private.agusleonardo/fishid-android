package com.ta.fishid.ui.splashscreen

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import com.ta.fishid.R
import com.ta.fishid.ui.base.BaseActivity
import com.ta.fishid.ui.login.LoginActivity
import com.ta.fishid.ui.main.MainActivity
import javax.inject.Inject

class SplashScreenActivity : BaseActivity() {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var viewModel: SplashScreenViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splashscreen)
        viewModel = initViewModel(SplashScreenViewModel::class.java, viewModelFactory)

        if (viewModel.preferencesHelper.rememberMe) startActivity(Intent(this, MainActivity::class.java))
        else startActivity(Intent(this, LoginActivity::class.java))
        finish()
    }

}