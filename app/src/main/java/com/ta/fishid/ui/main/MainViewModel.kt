package com.ta.fishid.ui.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.ta.fishid.cache.PreferencesHelper
import com.ta.fishid.repository.UserRepository
import com.ta.fishid.ui.base.BaseViewModel
import javax.inject.Inject

class MainViewModel @Inject constructor(
    val userRepository: UserRepository,
    val preferencesHelper: PreferencesHelper
) : BaseViewModel() {

    var currentPageLiveData: MutableLiveData<Int>? = null
        get() {
            if (field == null) {
                field = MutableLiveData()
            }
            return field
        }
}