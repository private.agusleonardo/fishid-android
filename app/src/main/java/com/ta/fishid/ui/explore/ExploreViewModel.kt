package com.ta.fishid.ui.explore

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.ta.fishid.Constants
import com.ta.fishid.cache.PreferencesHelper
import com.ta.fishid.repository.ExploreRepository
import com.ta.fishid.repository.UserRepository
import com.ta.fishid.ui.base.BaseViewModel
import javax.inject.Inject

class ExploreViewModel @Inject constructor(
    val exploreRepository: ExploreRepository,
    val preferencesHelper: PreferencesHelper
) : BaseViewModel() {
    var showLoading = ObservableBoolean(false)
    var showEmpty = ObservableBoolean(false)
    var showError = ObservableBoolean(false)
    var isLoading = ObservableBoolean(false)

    var checkFishTrigger: MutableLiveData<Boolean> = MutableLiveData()
    var checkExploreTrigger: MutableLiveData<Boolean> = MutableLiveData()
    var filter = ObservableBoolean(false)
    var readMore = ObservableBoolean(false)
    var page = ObservableInt(0)
    var perPage = ObservableInt(20)
    var name = ObservableField("")
    var foto = ObservableField("")
    var postId = ObservableField("")
    var fishId = ObservableField("")
    var userId = preferencesHelper.getAccountRx().blockingGet().userId!!

    var checkFilter = Transformations.switchMap(checkFishTrigger) {
        exploreRepository.getFish()
    }

    fun getFish() {
        checkFishTrigger.value = checkFishTrigger.value == false
    }

    var checkExplore = Transformations.switchMap(checkExploreTrigger) {
        exploreRepository.getExplore(fishId.get() ?: "", page.get(), perPage.get())
    }

    fun getExplore() {
        checkExploreTrigger.value = checkExploreTrigger.value == false
    }

    fun onRefresh() {
        setRefreshing(true)
        page.set(0)
        getExplore()
    }

    fun setRefreshing(action: Boolean) {
        isLoading.set(action)
    }

    fun setShowLoading(action: Boolean) {
        showLoading.set(action)
    }

    fun setShowError(action: Boolean) {
        showError.set(action)
    }

    fun setShowEmpty(action: Boolean) {
        showEmpty.set(action)
    }
}