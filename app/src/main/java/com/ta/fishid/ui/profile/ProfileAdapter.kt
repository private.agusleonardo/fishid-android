package com.ta.fishid.ui.profile

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ta.fishid.databinding.ItemProfilePostBinding
import com.ta.fishid.model.PostData
import com.ta.fishid.util.OnLoadMoreListener

class ProfileAdapter(
    val fragment: ProfileFragment?,
    val activity: ProfileActivity?,
    recyclerView: RecyclerView
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var data: MutableList<PostData> = arrayListOf()
    var lastVisibleItem = 0
    var totalItem = 0
    var isLoading = false
    var isComplete = false
    var onLoadMoreListener: OnLoadMoreListener? = null
    val visibleThresHold = 5

    init {
        val linearLayoutManager = recyclerView.layoutManager as LinearLayoutManager
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                totalItem = linearLayoutManager.itemCount
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition()

                if (!isComplete && !isLoading && totalItem < lastVisibleItem + visibleThresHold && dy > 0) {
                    if (onLoadMoreListener != null) {
                        onLoadMoreListener!!.onLoadMore()
                    }
                    isLoading = true
                }
            }
        })
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ItemProfilePostHolder(
            ItemProfilePostBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is ItemProfilePostHolder -> holder.bind(data[position])
        }
    }

    fun setOnloadMoreListener(onLoadMoreListener: OnLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener
    }

    inner class ItemProfilePostHolder(itemView: ItemProfilePostBinding) :
        RecyclerView.ViewHolder(itemView.root) {
        val binding = itemView
        fun bind(data: PostData) {
            if (fragment != null)
                binding.imageView.setOnClickListener {
                    fragment.intentPostDetail(data, adapterPosition)
                }
            else if (activity != null)
                binding.imageView.setOnClickListener {
                    activity.intentPostDetail(data, adapterPosition)
                }
            binding.data = data
        }
    }
}