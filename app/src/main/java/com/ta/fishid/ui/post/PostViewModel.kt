package com.ta.fishid.ui.post

import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.ta.fishid.cache.PreferencesHelper
import com.ta.fishid.model.DetectData
import com.ta.fishid.repository.PostRepository
import com.ta.fishid.repository.UserRepository
import com.ta.fishid.ui.base.BaseViewModel
import com.ta.fishid.util.Utility.createFormDataString
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import javax.inject.Inject

class PostViewModel @Inject constructor(
    val postRepository: PostRepository,
    val preferencesHelper: PreferencesHelper
) : BaseViewModel() {
    var data: List<DetectData> = arrayListOf()
    var checkPostTrigger: MutableLiveData<Boolean> = MutableLiveData()
    var checkDetectPostTrigger: MutableLiveData<Boolean> = MutableLiveData()
    var showAdditional = ObservableField(false)
    var caption = ""
    var selectedFish = -1
    var selectedFishId = ""
    var selectedBentukIkan = ""
    var selectedMulutIkan = ""
    var selectedSiripIkan = ""
    var selectedEkorIkan = ""
    var selectedCorakIkan = ""
    var indexSelectedFish = 0

    private var body: MultipartBody.Part? = null

    var checkPost = Transformations.switchMap(checkPostTrigger) {
        val fishId: ArrayList<MultipartBody.Part> = arrayListOf()
        val x1: ArrayList<MultipartBody.Part> = arrayListOf()
        val y1: ArrayList<MultipartBody.Part> = arrayListOf()
        val x2: ArrayList<MultipartBody.Part> = arrayListOf()
        val y2: ArrayList<MultipartBody.Part> = arrayListOf()
        val probability: ArrayList<MultipartBody.Part> = arrayListOf()
        val status: ArrayList<MultipartBody.Part> = arrayListOf()

        data.forEach {
            fishId.add(createFormDataString("fish_id", it.fishId))
            x1.add(createFormDataString("x1", it.x1.toString()))
            y1.add(createFormDataString("y1", it.y1.toString()))
            x2.add(createFormDataString("x2", it.x2.toString()))
            y2.add(createFormDataString("y2", it.y2.toString()))
            probability.add(createFormDataString("probability", it.probability.toString()))
            if (selectedFish == indexSelectedFish++) status.add(createFormDataString("status", "true"))
            else status.add(createFormDataString("status", "false"))
        }

        postRepository.postPost(
            preferencesHelper.getAccountRx().blockingGet().userId!!,
            caption,
            selectedBentukIkan,
            selectedMulutIkan,
            selectedSiripIkan,
            selectedEkorIkan,
            selectedCorakIkan,
            fishId,
            x1,
            y1,
            x2,
            y2,
            probability,
            status,
            body!!
        )
    }

    fun postPost(file: File) {
        val requestFile = RequestBody.create("image/*".toMediaTypeOrNull(), file)

        body = MultipartBody.Part.createFormData("post", file.name, requestFile)
        checkPostTrigger.value = checkPostTrigger.value == false
    }

    var checkDetectPost = Transformations.switchMap(checkDetectPostTrigger) {
        postRepository.postDetect(
            body!!
        )
    }

    fun postDetect(file: File) {
        val requestFile = RequestBody.create("image/*".toMediaTypeOrNull(), file)

        body = MultipartBody.Part.createFormData("post", file.name, requestFile)
        checkDetectPostTrigger.value = checkDetectPostTrigger.value == false
    }
}