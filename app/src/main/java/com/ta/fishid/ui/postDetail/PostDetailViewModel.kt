package com.ta.fishid.ui.postDetail

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.ta.fishid.cache.PreferencesHelper
import com.ta.fishid.repository.HomeRepository
import com.ta.fishid.repository.PostRepository
import com.ta.fishid.repository.UserRepository
import com.ta.fishid.ui.base.BaseViewModel
import javax.inject.Inject

class PostDetailViewModel @Inject constructor(
    val postRepository: PostRepository,
    val preferencesHelper: PreferencesHelper
) : BaseViewModel() {
    var showLoading = ObservableBoolean(false)
    var showEmpty = ObservableBoolean(false)
    var showError = ObservableBoolean(false)
    var isLoading = ObservableBoolean(false)

    var userId = preferencesHelper.getAccountRx().blockingGet().userId!!
    var postId = ObservableField("")
    var reason = ObservableField("")
    var checkPostTrigger: MutableLiveData<Boolean> = MutableLiveData()
    var checkLikeTrigger: MutableLiveData<Boolean> = MutableLiveData()
    var checkUnlikeTrigger: MutableLiveData<Boolean> = MutableLiveData()
    var checkReportTrigger: MutableLiveData<Boolean> = MutableLiveData()
    var checkDeleteTrigger: MutableLiveData<Boolean> = MutableLiveData()

    var checkPost = Transformations.switchMap(checkPostTrigger) {
        postRepository.getPost(postId.get()!!)
    }

    fun getPost() {
        checkPostTrigger.value = checkPostTrigger.value == false
    }

    var checkLikePost = Transformations.switchMap(checkLikeTrigger) {
        postRepository.likePost(userId, postId.get()!!)
    }

    fun likePost(postId: String) {
        this.postId.set(postId)
        checkLikeTrigger.value = checkLikeTrigger.value == false
    }

    var checkUnlikePost = Transformations.switchMap(checkUnlikeTrigger) {
        postRepository.unlikePost(userId, postId.get()!!)
    }

    fun unlikePost(postId: String) {
        this.postId.set(postId)
        checkUnlikeTrigger.value = checkUnlikeTrigger.value == false
    }

    var checkReport = Transformations.switchMap(checkReportTrigger) {
        postRepository.report(userId, postId.get()!!, reason.get()!!)
    }

    fun report(postId: String, reason: String) {
        this.postId.set(postId)
        this.reason.set(reason)
        checkReportTrigger.value = checkReportTrigger.value == false
    }


    var checkDelete = Transformations.switchMap(checkDeleteTrigger) {
        postRepository.delete(userId, postId.get()!!)
    }

    fun delete(postId: String) {
        this.postId.set(postId)
        checkDeleteTrigger.value = checkDeleteTrigger.value == false
    }
}