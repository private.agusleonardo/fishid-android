package com.ta.fishid

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import com.ta.fishid.api.model.RequestHeader
import com.ta.fishid.dagger.DaggerAppComponent
import com.ta.fishid.repository.UserRepository
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication
import javax.inject.Inject

class FishIdApp : DaggerApplication() {
    @Inject
    lateinit var requestHeader: RequestHeader
    @Inject
    lateinit var userRepository: UserRepository

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerAppComponent.builder().create(this)
    }

    override fun onCreate() {
        super.onCreate()
        initializeApplication()
        createNotificationChannel()
    }

    private fun initializeApplication() {
        //load the current user into the system
        val token = userRepository.preferencesHelper.getJwtToken()

        //load the current access token into all requests
        if (token.isNotEmpty()) requestHeader.accesstoken.accessToken = token

    }

    private fun createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val descriptionText = "Notifikasi untuk Supplier"
            val importance = NotificationManager.IMPORTANCE_HIGH

            val supplierChannel = NotificationChannel(
                Constants.ChannelSupplier,
                "Supplier",
                importance
            ).apply {
                description = descriptionText
            }

            val notificationManager: NotificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(supplierChannel)

        }


    }

}