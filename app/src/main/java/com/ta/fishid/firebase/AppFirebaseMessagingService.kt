package com.ta.fishid.firebase

import android.content.ContentValues.TAG
import android.graphics.Color
import android.media.RingtoneManager
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.ta.fishid.Constants
import com.ta.fishid.FishIdApp
import com.ta.fishid.R
import com.ta.fishid.cache.PreferencesHelper
import com.ta.fishid.repository.UserRepository
import javax.inject.Inject

class AppFirebaseMessagingService : FirebaseMessagingService() {
    @Inject
    lateinit var userRepository: UserRepository
    @Inject
    lateinit var preferencesHelper: PreferencesHelper

    override fun onCreate() {
        super.onCreate()
        (application as FishIdApp).serviceInjector().inject(this)
        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.d("FCM", "getInstanceId failed", task.exception)
                    return@OnCompleteListener
                }

                // Get new Instance ID token
                val token = task.result?.token
                saveToken(token ?: "")
                Log.d(" Token : ", token ?: "")
            })
    }

    override fun onNewToken(p0: String) {
        super.onNewToken(p0)
        saveToken(p0)
    }

    private fun saveToken(token: String) {
        Log.d("FCM Token", token)

        preferencesHelper.setFcmToken(token)

        if (preferencesHelper.rememberMe) {
            userRepository.postFcmToken(
                preferencesHelper.getAccountRx().blockingGet().username ?: "",
                token
            )
        }
    }

    override fun onMessageReceived(p0: RemoteMessage) {
        Log.d(TAG, "From: " + p0.from)

        // Check if message contains a data payload.
        if (p0.data.isNotEmpty()) {
            Log.d(TAG, "Message data payload: " + p0.data);
        }

        // Check if message contains a notification payload.
        if (p0.notification != null) {
            Log.d(TAG, "Message Notification Body: " + p0.notification?.body)
            pushNotification(p0.notification?.title ?: "", p0.notification?.body ?: "")
        }


    }

    private fun pushNotification(title: String, body: String) {
        val notifyManager = NotificationManagerCompat.from(this)

        val summary = title
        val summaryContent = body
        val notificationBuilder = NotificationCompat.Builder(this, Constants.ChannelSupplier)
            .setSmallIcon(R.drawable.ic_logo)
            .setContentTitle(summary)
            .setContentText(summaryContent)
            .setAutoCancel(true)
//            .setContentIntent(pendingIntentRoom)
            .setPriority(NotificationCompat.PRIORITY_MAX)
            .setGroupAlertBehavior(NotificationCompat.GROUP_ALERT_SUMMARY)
            .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
            .setVibrate(longArrayOf(1000, 1000))
            .setLights(Color.BLUE, 3000, 3000)
            .build()

        notifyManager.notify(0, notificationBuilder)
    }

}