package com.ta.fishid.repository

import androidx.lifecycle.LiveData
import com.ta.fishid.api.ApiService
import com.ta.fishid.api.ApiSuccessResponse
import com.ta.fishid.api.model.*
import com.ta.fishid.cache.PreferencesHelper
import com.ta.fishid.model.CommentData
import com.ta.fishid.model.DetectData
import com.ta.fishid.model.LocalUserData
import com.ta.fishid.model.PostData
import com.ta.fishid.util.AppExecutors
import com.ta.fishid.util.NetworkBoundResource
import com.ta.fishid.util.Utility
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.util.HashMap
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PostRepository @Inject constructor(
    private val appExecutors: AppExecutors,
    val preferencesHelper: PreferencesHelper,
    private val APIService: ApiService,
    private val requestHeaders: RequestHeader,
    private val localUserData: LocalUserData
) {
    fun postDetect(
        picture: MultipartBody.Part
    ): LiveData<Resource<List<DetectData>>> {
        return object :
            NetworkBoundResource<List<DetectData>, BaseListResponse<DetectData>>(appExecutors) {
            override fun processResult(item: BaseListResponse<DetectData>): List<DetectData>? {
                return item.data
            }

            override fun createCall() =
                APIService.postDetect(
                    preferencesHelper.getAccountRx().blockingGet().userId!!,
                    picture
                )

            override fun processResponse(response: ApiSuccessResponse<BaseListResponse<DetectData>>)
                    : BaseListResponse<DetectData> {
                val body = response.body
                return body
            }

        }.asLiveData()
    }

    fun postPost(
        userId: String,
        caption: String,
        bentuk_tubuh: String,
        bentuk_mulut: String,
        bentuk_sirip: String,
        bentuk_ekor: String,
        corak_tubuh: String,
        fishId: List<MultipartBody.Part>,
        x1: List<MultipartBody.Part>,
        y1: List<MultipartBody.Part>,
        x2: List<MultipartBody.Part>,
        y2: List<MultipartBody.Part>,
        probability: List<MultipartBody.Part>,
        status: List<MultipartBody.Part>,
        picture: MultipartBody.Part
    ): LiveData<Resource<BaseResponse>> {
        return object :
            NetworkBoundResource<BaseResponse, BaseResponse>(appExecutors) {
            override fun processResult(item: BaseResponse): BaseResponse? {
                return item
            }

            override fun createCall() =
                APIService.postPost(
                    picture,
                    fishId,
                    x1,
                    y1,
                    x2,
                    y2,
                    probability,
                    status,
                    postParams(
                        userId,
                        caption,
                        bentuk_tubuh,
                        bentuk_mulut,
                        bentuk_sirip,
                        bentuk_ekor,
                        corak_tubuh
                    )
                )

            override fun processResponse(response: ApiSuccessResponse<BaseResponse>)
                    : BaseResponse {
                val body = response.body
                return body
            }

        }.asLiveData()
    }

    private fun postParams(
        userId: String,
        caption: String,
        bentuk_tubuh: String,
        bentuk_mulut: String,
        bentuk_sirip: String,
        bentuk_ekor: String,
        corak_tubuh: String
    ): HashMap<String?, RequestBody?>? {
        val params = HashMap<String?, RequestBody?>()
        params["user_id"] = Utility.createPartFromString(userId)
        params["caption"] = Utility.createPartFromString(caption)
        params["bentuk_tubuh"] = Utility.createPartFromString(bentuk_tubuh)
        params["bentuk_mulut"] = Utility.createPartFromString(bentuk_mulut)
        params["bentuk_sirip"] = Utility.createPartFromString(bentuk_sirip)
        params["bentuk_ekor"] = Utility.createPartFromString(bentuk_ekor)
        params["corak_tubuh"] = Utility.createPartFromString(corak_tubuh)
        return params
    }

    fun getPost(postId: String): LiveData<Resource<PostData>> {
        return object :
            NetworkBoundResource<PostData, BaseDataResponse<PostData>>(appExecutors) {
            override fun processResult(item: BaseDataResponse<PostData>): PostData? {
                return item.data
            }

            override fun createCall() =
                APIService.getPost(preferencesHelper.getAccountRx().blockingGet().userId!!, postId)

            override fun processResponse(response: ApiSuccessResponse<BaseDataResponse<PostData>>)
                    : BaseDataResponse<PostData> {
                val body = response.body
                return body
            }

        }.asLiveData()
    }

    fun getComment(postId: String): LiveData<Resource<CommentData>> {
        return object :
            NetworkBoundResource<CommentData, BaseDataResponse<CommentData>>(appExecutors) {
            override fun processResult(item: BaseDataResponse<CommentData>): CommentData? {
                return item.data
            }

            override fun createCall() =
                APIService.getComment(postId)

            override fun processResponse(response: ApiSuccessResponse<BaseDataResponse<CommentData>>)
                    : BaseDataResponse<CommentData> {
                val body = response.body
                return body
            }

        }.asLiveData()
    }

    fun postComment(
        postId: String,
        comment: String,
        time: String
    ): LiveData<Resource<BaseResponse>> {
        return object :
            NetworkBoundResource<BaseResponse, BaseResponse>(appExecutors) {
            override fun processResult(item: BaseResponse): BaseResponse? {
                return item
            }

            override fun createCall() =
                APIService.postComment(
                    commentParams(
                        preferencesHelper.getAccountRx().blockingGet().userId!!,
                        postId,
                        comment,
                        time
                    )
                )

            override fun processResponse(response: ApiSuccessResponse<BaseResponse>)
                    : BaseResponse {
                val body = response.body
                return body
            }

        }.asLiveData()
    }

    private fun commentParams(
        userId: String,
        postId: String,
        comment: String,
        time: String
    ): Map<String?, String?>? {
        val params = HashMap<String?, String?>()
        params["user_id"] = userId
        params["post_id"] = postId
        params["comment"] = comment
        params["waktu"] = time
        return params
    }

    fun delete(
        userId: String,
        postId: String
    ): LiveData<Resource<BaseResponse>> {
        return object :
            NetworkBoundResource<BaseResponse, BaseResponse>(appExecutors) {
            override fun processResult(item: BaseResponse): BaseResponse? {
                return item
            }

            override fun createCall() =
                APIService.delete(
                    likeParams(
                        userId,
                        postId
                    )
                )

            override fun processResponse(response: ApiSuccessResponse<BaseResponse>)
                    : BaseResponse {
                val body = response.body
                return body
            }

        }.asLiveData()
    }

    fun likePost(
        userId: String,
        postId: String
    ): LiveData<Resource<BaseResponse>> {
        return object :
            NetworkBoundResource<BaseResponse, BaseResponse>(appExecutors) {
            override fun processResult(item: BaseResponse): BaseResponse? {
                return item
            }

            override fun createCall() =
                APIService.like(
                    likeParams(
                        userId,
                        postId
                    )
                )

            override fun processResponse(response: ApiSuccessResponse<BaseResponse>)
                    : BaseResponse {
                val body = response.body
                return body
            }

        }.asLiveData()
    }

    fun unlikePost(
        userId: String,
        postId: String
    ): LiveData<Resource<BaseResponse>> {
        return object :
            NetworkBoundResource<BaseResponse, BaseResponse>(appExecutors) {
            override fun processResult(item: BaseResponse): BaseResponse? {
                return item
            }

            override fun createCall() =
                APIService.unlike(
                    likeParams(
                        userId,
                        postId
                    )
                )

            override fun processResponse(response: ApiSuccessResponse<BaseResponse>)
                    : BaseResponse {
                val body = response.body
                return body
            }

        }.asLiveData()
    }

    private fun likeParams(
        userId: String,
        postId: String
    ): Map<String?, String?>? {
        val params = HashMap<String?, String?>()
        params["user_id"] = userId
        params["post_id"] = postId
        return params
    }

    fun disableComment(
        postId: String,
        status: Boolean
    ): LiveData<Resource<BaseResponse>> {
        return object :
            NetworkBoundResource<BaseResponse, BaseResponse>(appExecutors) {
            override fun processResult(item: BaseResponse): BaseResponse? {
                return item
            }

            override fun createCall() =
                APIService.disableComment(
                    disableCommentParams(
                        postId,
                        status
                    )
                )

            override fun processResponse(response: ApiSuccessResponse<BaseResponse>)
                    : BaseResponse {
                val body = response.body
                return body
            }

        }.asLiveData()
    }

    private fun disableCommentParams(
        postId: String,
        status: Boolean
    ): Map<String?, String?>? {
        val params = HashMap<String?, String?>()
        params["post_id"] = postId
        params["comment_status"] = status.toString()
        return params
    }

    fun report(
        userId: String,
        postId: String,
        reason: String
    ): LiveData<Resource<BaseResponse>> {
        return object :
            NetworkBoundResource<BaseResponse, BaseResponse>(appExecutors) {
            override fun processResult(item: BaseResponse): BaseResponse? {
                return item
            }

            override fun createCall() =
                APIService.report(
                    reasonParams(
                        userId,
                        postId,
                        reason
                    )
                )

            override fun processResponse(response: ApiSuccessResponse<BaseResponse>)
                    : BaseResponse {
                val body = response.body
                return body
            }

        }.asLiveData()
    }

    private fun reasonParams(
        userId: String,
        postId: String,
        reason: String
    ): Map<String?, String?>? {
        val params = HashMap<String?, String?>()
        params["user_id"] = userId
        params["post_id"] = postId
        params["reason"] = reason
        return params
    }
}