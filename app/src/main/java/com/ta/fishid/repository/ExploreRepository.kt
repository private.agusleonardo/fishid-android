package com.ta.fishid.repository

import androidx.lifecycle.LiveData
import com.ta.fishid.api.ApiService
import com.ta.fishid.api.ApiSuccessResponse
import com.ta.fishid.api.model.BaseDataResponse
import com.ta.fishid.api.model.BaseListResponse
import com.ta.fishid.api.model.RequestHeader
import com.ta.fishid.api.model.Resource
import com.ta.fishid.cache.PreferencesHelper
import com.ta.fishid.model.ExploreData
import com.ta.fishid.model.FilterData
import com.ta.fishid.model.LocalUserData
import com.ta.fishid.util.AppExecutors
import com.ta.fishid.util.NetworkBoundResource
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ExploreRepository @Inject constructor(
    private val appExecutors: AppExecutors,
    val preferencesHelper: PreferencesHelper,
    private val APIService: ApiService,
    private val requestHeaders: RequestHeader,
    private val localUserData: LocalUserData
) {
    fun getExplore(fishId: String, page: Int, perPage: Int): LiveData<Resource<ExploreData>> {
        return object :
            NetworkBoundResource<ExploreData, BaseDataResponse<ExploreData>>(appExecutors) {
            override fun processResult(item: BaseDataResponse<ExploreData>): ExploreData? {
                return item.data
            }

            override fun createCall() =
                APIService.getExplore(fishId, page.toString(), perPage.toString())

            override fun processResponse(response: ApiSuccessResponse<BaseDataResponse<ExploreData>>)
                    : BaseDataResponse<ExploreData> {
                val body = response.body
                return body
            }

        }.asLiveData()
    }

    fun getFish(): LiveData<Resource<List<FilterData>>> {
        return object :
            NetworkBoundResource<List<FilterData>, BaseListResponse<FilterData>>(appExecutors) {
            override fun processResult(item: BaseListResponse<FilterData>): List<FilterData>? {
                return item.data
            }

            override fun createCall() =
                APIService.getFish()

            override fun processResponse(response: ApiSuccessResponse<BaseListResponse<FilterData>>)
                    : BaseListResponse<FilterData> {
                val body = response.body
                return body
            }

        }.asLiveData()
    }
}