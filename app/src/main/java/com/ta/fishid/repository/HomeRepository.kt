package com.ta.fishid.repository

import androidx.lifecycle.LiveData
import com.ta.fishid.api.ApiService
import com.ta.fishid.api.ApiSuccessResponse
import com.ta.fishid.api.model.BaseListResponse
import com.ta.fishid.api.model.BaseResponse
import com.ta.fishid.api.model.RequestHeader
import com.ta.fishid.api.model.Resource
import com.ta.fishid.cache.PreferencesHelper
import com.ta.fishid.model.LocalUserData
import com.ta.fishid.model.PostData
import com.ta.fishid.util.AppExecutors
import com.ta.fishid.util.NetworkBoundResource
import java.util.HashMap
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class HomeRepository @Inject constructor(
    private val appExecutors: AppExecutors,
    val preferencesHelper: PreferencesHelper,
    private val APIService: ApiService,
    private val requestHeaders: RequestHeader,
    private val localUserData: LocalUserData
) {
    fun getHome(page: Int, perPage: Int, offset:Int): LiveData<Resource<List<PostData>>> {
        return object :
            NetworkBoundResource<List<PostData>, BaseListResponse<PostData>>(appExecutors) {
            override fun processResult(item: BaseListResponse<PostData>): List<PostData>? {
                return item.data
            }

            override fun createCall() =
                APIService.getHome(
                    preferencesHelper.getAccountRx().blockingGet().userId!!,
                    page.toString(),
                    perPage.toString(),
                    offset.toString()
                )

            override fun processResponse(response: ApiSuccessResponse<BaseListResponse<PostData>>)
                    : BaseListResponse<PostData> {
                return response.body
            }

        }.asLiveData()
    }

}