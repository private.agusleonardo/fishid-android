package com.ta.fishid.repository

import androidx.lifecycle.LiveData
import com.ta.fishid.api.ApiService
import com.ta.fishid.api.ApiSuccessResponse
import com.ta.fishid.api.BaseRx
import com.ta.fishid.api.model.*
import com.ta.fishid.cache.PreferencesHelper
import com.ta.fishid.model.*
import com.ta.fishid.util.AppExecutors
import com.ta.fishid.util.NetworkBoundResource
import com.ta.fishid.util.Utility.createPartFromString
import io.reactivex.Single
import io.reactivex.observers.DisposableObserver
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserRepository @Inject constructor(
    private val appExecutors: AppExecutors,
    val preferencesHelper: PreferencesHelper,
    private val APIService: ApiService,
    private val requestHeaders: RequestHeader,
    private val localUserData: LocalUserData
) {

    fun getLocalUserData(): Single<UserData> {
        return preferencesHelper.getAccountRx()
    }

    fun register(
        userId: String,
        bio: String,
        password: String
    ): LiveData<Resource<UserData>> {
        return object : NetworkBoundResource<UserData, BaseDataResponse<UserData>>(appExecutors) {
            override fun processResult(item: BaseDataResponse<UserData>): UserData? {
                preferencesHelper.saveAccount(item.data!!)
                return item.data
            }

            override fun createCall() =
                APIService.register(
                    registerParams(userId, bio, password)
                )

            override fun processResponse(response: ApiSuccessResponse<BaseDataResponse<UserData>>)
                    : BaseDataResponse<UserData> {
                val body = response.body
                val header = response.links
                preferencesHelper.setJwtToken(header["jwt_token"] ?: "")
                requestHeaders.accesstoken.accessToken = header["jwt_token"] ?: ""
//                Log.d("jwt_token", header["jwt_token"] ?: "")
                return body
            }
        }.asLiveData()
    }

    private fun registerParams(
        username: String?,
        bio: String,
        password: String?
    ): Map<String?, String?>? {
        val params = HashMap<String?, String?>()
        params["username"] = username
        params["bio"] = bio
        params["password"] = password
        return params
    }

    fun login(
        username: String,
        password: String
    ): LiveData<Resource<UserData>> {
        return object : NetworkBoundResource<UserData, BaseDataResponse<UserData>>(appExecutors) {
            override fun processResult(item: BaseDataResponse<UserData>): UserData? {
                preferencesHelper.saveAccount(item.data!!)
                return item.data
            }

            override fun createCall() =
                APIService.login(
                    loginParams(username, password)
                )

            override fun processResponse(response: ApiSuccessResponse<BaseDataResponse<UserData>>)
                    : BaseDataResponse<UserData> {
                val body = response.body
                val header = response.links
                preferencesHelper.setJwtToken(header["jwt_token"] ?: "")
                requestHeaders.accesstoken.accessToken = header["jwt_token"] ?: ""
//                Log.d("jwt_token", header["jwt_token"] ?: "")
                return body
            }
        }.asLiveData()
    }

    private fun loginParams(
        username: String?,
        password: String?
    ): Map<String?, String?>? {
        val params = HashMap<String?, String?>()
        params["username"] = username
        params["password"] = password
        return params
    }

    fun postFcmToken(userId: String, fcmId: String) {
        val disposableObserver = object : DisposableObserver<BaseResponse>() {
            override fun onError(e: Throwable) {}
            override fun onNext(t: BaseResponse) {}
            override fun onComplete() {}
        }
        val observable = APIService.postFirebaseToken(fcmParams(userId, fcmId))

        BaseRx().requestNoList(observable, disposableObserver)
    }

    private fun fcmParams(
        userId: String?,
        password: String?
    ): Map<String?, String?>? {
        val params = HashMap<String?, String?>()
        params["user_id"] = userId
        params["token"] = password
        return params
    }

    fun getProfile(
        userId: String,
        page: Int,
        perPage: Int,
        offset: Int
    ): LiveData<Resource<ProfileData>> {
        return object :
            NetworkBoundResource<ProfileData, BaseDataResponse<ProfileData>>(appExecutors) {
            override fun processResult(item: BaseDataResponse<ProfileData>): ProfileData? {
                return item.data
            }

            override fun createCall() =
                APIService.getProfile(userId, page.toString(), perPage.toString(), offset.toString())

            override fun processResponse(response: ApiSuccessResponse<BaseDataResponse<ProfileData>>)
                    : BaseDataResponse<ProfileData> {
                val body = response.body
                val header = response.links
                preferencesHelper.setJwtToken(header["jwt_token"] ?: "")
                requestHeaders.accesstoken.accessToken = header["jwt_token"] ?: ""
//                Log.d("jwt_token", header["jwt_token"] ?: "")
                return body
            }
        }.asLiveData()
    }

    fun postProfile(
        userId: String,
        username: String,
        bio: String,
        picture: MultipartBody.Part
    ): LiveData<Resource<UserData>> {
        return object :
            NetworkBoundResource<UserData, BaseDataResponse<UserData>>(appExecutors) {
            override fun processResult(item: BaseDataResponse<UserData>): UserData? {
                return item.data
            }

            override fun createCall() =
                APIService.postProfile(
                    userId,
                    picture,
                    profileParams(
                        username,
                        bio
                    )
                )

            override fun processResponse(response: ApiSuccessResponse<BaseDataResponse<UserData>>)
                    : BaseDataResponse<UserData> {
                val body = response.body
                return body
            }

        }.asLiveData()
    }

    fun postProfile(
        userId: String,
        username: String,
        bio: String
    ): LiveData<Resource<UserData>> {
        return object :
            NetworkBoundResource<UserData, BaseDataResponse<UserData>>(appExecutors) {
            override fun processResult(item: BaseDataResponse<UserData>): UserData? {
                return item.data
            }

            override fun createCall() =
                APIService.postProfile(
                    userId,
                    profileParams(
                        username,
                        bio
                    )
                )

            override fun processResponse(response: ApiSuccessResponse<BaseDataResponse<UserData>>)
                    : BaseDataResponse<UserData> {
                val body = response.body
                return body
            }

        }.asLiveData()
    }

    private fun profileParams(
        username: String,
        bio: String
    ): HashMap<String?, RequestBody?>? {
        val params = HashMap<String?, RequestBody?>()
        params["username"] = createPartFromString(username)
        params["bio"] = createPartFromString(bio)
        return params
    }

    fun postPassword(
        userId: String,
        oldPassword: String,
        newPassword: String
    ): LiveData<Resource<BaseResponse>> {
        return object :
            NetworkBoundResource<BaseResponse, BaseResponse>(appExecutors) {
            override fun processResult(item: BaseResponse): BaseResponse? {
                return item
            }

            override fun createCall() =
                APIService.postPassword(
                    passwordParams(
                        userId,
                        oldPassword,
                        newPassword
                    )
                )

            override fun processResponse(response: ApiSuccessResponse<BaseResponse>)
                    : BaseResponse {
                val body = response.body
                return body
            }

        }.asLiveData()
    }

    private fun passwordParams(
        userId: String,
        oldPassword: String,
        newPassword: String
    ): Map<String?, String?>? {
        val params = HashMap<String?, String?>()
        params["user_id"] = userId
        params["old_password"] = oldPassword
        params["new_password"] = newPassword
        return params
    }

}