package com.ta.fishid.repository

import androidx.lifecycle.LiveData
import com.ta.fishid.api.ApiService
import com.ta.fishid.api.ApiSuccessResponse
import com.ta.fishid.api.model.BaseDataResponse
import com.ta.fishid.api.model.BaseListResponse
import com.ta.fishid.api.model.RequestHeader
import com.ta.fishid.api.model.Resource
import com.ta.fishid.cache.PreferencesHelper
import com.ta.fishid.model.HistoryData
import com.ta.fishid.model.LocalUserData
import com.ta.fishid.model.ProfileData
import com.ta.fishid.util.AppExecutors
import com.ta.fishid.util.NetworkBoundResource
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CollectionRepository @Inject constructor(
    private val appExecutors: AppExecutors,
    val preferencesHelper: PreferencesHelper,
    private val APIService: ApiService,
    private val requestHeaders: RequestHeader,
    private val localUserData: LocalUserData
) {
    fun getCollectionLike(
        userId: String
    ): LiveData<Resource<ProfileData>> {
        return object :
            NetworkBoundResource<ProfileData, BaseDataResponse<ProfileData>>(appExecutors) {
            override fun processResult(item: BaseDataResponse<ProfileData>): ProfileData? {
                return item.data
            }

            override fun createCall() =
                APIService.getCollectionLike(userId)

            override fun processResponse(response: ApiSuccessResponse<BaseDataResponse<ProfileData>>)
                    : BaseDataResponse<ProfileData> {
                val body = response.body
                return body
            }
        }.asLiveData()
    }

    fun getCollectionHistory(
        userId: String
    ): LiveData<Resource<List<HistoryData>>> {
        return object :
            NetworkBoundResource<List<HistoryData>, BaseListResponse<HistoryData>>(appExecutors) {
            override fun processResult(item: BaseListResponse<HistoryData>): List<HistoryData>? {
                return item.data
            }

            override fun createCall() =
                APIService.getCollectionHistory(userId)

            override fun processResponse(response: ApiSuccessResponse<BaseListResponse<HistoryData>>)
                    : BaseListResponse<HistoryData> {
                val body = response.body
                return body
            }
        }.asLiveData()
    }

}