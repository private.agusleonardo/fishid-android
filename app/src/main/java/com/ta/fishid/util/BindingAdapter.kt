package com.ta.fishid.util

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.ta.fishid.Constants
import com.ta.fishid.R

object BindingAdapter {
    @JvmStatic
    @BindingAdapter("addSrc")
    fun addSrc(view: ImageView, show: Int) {
        view.setBackgroundResource(show)
    }

    @BindingAdapter("addGlideImage")
    @JvmStatic
    fun addGlideImage(view: ImageView, txt: String?) {
        GlideApp.with(view.context)
            .load(txt)
            .placeholder(R.drawable.animation_progress)
            .override(300, 300)
            .error(R.drawable.ic_account)
            .into(view)
    }

    @BindingAdapter("addHomeProfileImage")
    @JvmStatic
    fun addHomeProfileImage(view: ImageView, txt: String?) {
        if (txt!!.isNotEmpty()) {
            Glide.with(view.context)
                .load(Constants.BaseURL + txt)
                .override(150, 150)
                .error(R.drawable.ic_account)
                .into(view)
        } else {
            Glide.with(view.context)
                .load(R.drawable.ic_account)
                .override(150, 150)
                .into(view)
        }
    }

    @BindingAdapter("addHomePostImage")
    @JvmStatic
    fun addHomePostImage(view: ImageView, txt: String?) {
        GlideApp.with(view.context)
            .load(Constants.BaseURL + txt)
            .into(view)
    }

    @BindingAdapter("addFilterImage")
    @JvmStatic
    fun addFilterImage(view: ImageView, txt: String?) {
        if (txt!!.isNotEmpty())
            GlideApp.with(view.context)
                .load(Constants.BaseURL + txt)
                .override(
                    com.bumptech.glide.request.target.Target.SIZE_ORIGINAL,
                    com.bumptech.glide.request.target.Target.SIZE_ORIGINAL
                )
                .into(view)
        else view.setImageResource(R.drawable.fish_other)
    }

}