package com.ta.fishid.util

import androidx.annotation.MainThread
import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import com.ta.fishid.api.*
import com.ta.fishid.api.model.Resource

abstract class NetworkBoundResource<ResultType, RequestType>
@MainThread constructor(private val appExecutors: AppExecutors) {

    private val result = MediatorLiveData<Resource<ResultType>>()

    init {
        result.value = Resource.loading(null)
        @Suppress("LeakingThis")
        fetchFromNetwork()
    }

    @MainThread
    private fun setValue(newValue: Resource<ResultType>) {
        if (result.value != newValue) {
            result.value = newValue
        }
    }

    private fun fetchFromNetwork() {
        val apiResponse = createCall()
        // we re-attach dbSource as a new source, it will dispatch its latest value quickly
        result.addSource(apiResponse) { response ->
            result.removeSource(apiResponse)
            when (response) {
                is ApiSuccessResponse -> {
                    appExecutors.diskIO().execute {
                        appExecutors.mainThread().execute {
                            val loadFromNetwork = MutableLiveData<ResultType>()
                            loadFromNetwork.value = processResult(processResponse(response))
                            result.addSource(loadFromNetwork) { newData ->
                                setValue(Resource.success(newData))
                            }
                        }
                    }
                }
                is ApiEmptyResponse -> {
                    appExecutors.mainThread().execute {
                        setValue(Resource.empty())
                    }
                }

                is ApiAcceptedResponse -> {
                    appExecutors.mainThread().execute {
                        setValue(Resource.accepted())
                    }
                }

                is ApiUnauthorizedResponse -> {
                    onFetchFailed()
                    setValue(Resource.unauthorized(response.errorMessage, null))
                }

                is ApiValidationErrorResponse -> {
                    onFetchFailed()
                    setValue(Resource.validationError(response.errorMessage, null))
                }

                is ApiErrorResponse -> {
                    onFetchFailed()
                    setValue(Resource.error(response.errorMessage, null))
                }

                is ApiGoneResponse -> {
                    onFetchFailed()
                    setValue(Resource.gone(response.errorMessage, null))
                }
            }
        }
    }

    protected open fun onFetchFailed() {}

    fun asLiveData() = result as LiveData<Resource<ResultType>>

    @WorkerThread
    protected abstract fun processResult(item: RequestType): ResultType?

    @WorkerThread
    protected open fun processResponse(response: ApiSuccessResponse<RequestType>) = response.body

    @MainThread
    protected abstract fun createCall(): LiveData<ApiResponse<RequestType>>
}