package com.ta.fishid.util

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.ContextWrapper
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Build
import android.text.Html
import android.text.Spanned
import android.util.Log
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.webkit.WebView
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.bumptech.glide.Glide
import com.ta.fishid.R
import com.ta.fishid.util.zoomEvent.ScaleListener
import kotlinx.android.synthetic.main.dialog_custom_check.*
import kotlinx.android.synthetic.main.dialog_custom_check.tv_text
import kotlinx.android.synthetic.main.dialog_custom_prompt.*
import kotlinx.android.synthetic.main.dialog_info.*
import kotlinx.android.synthetic.main.dialog_report.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.OutputStream
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*

object Utility {
    private val TAG = "Utility"
    private val currentDate = Calendar.getInstance().time

    fun log(message: String) {
        Log.d("Utility", message)
    }

    fun convertDateToCalendar(date: Date?): Calendar {
        val cal = Calendar.getInstance()
        cal.time = date!!
        return cal
    }

    fun convertIntToDecimal(a: Int?): String {
        val pattern = "###,###"
        val decimalFormat = DecimalFormat(pattern)
        return decimalFormat.format(a).replace(",", ".")
    }

    fun convertIntToRupiah(a: Int?): String {
        val pattern = "Rp ###,###"
        val decimalFormat = DecimalFormat(pattern)
        return decimalFormat.format(a)
    }

    fun convertStringToDate(type: Int, dateTxt: String): Date {
        val timezone = TimeZone.getDefault().getDisplayName(false, TimeZone.SHORT)
        val parser = when (type) {
            1 -> SimpleDateFormat("E MMM dd HH:mm:ss $timezone yyyy", Locale.ENGLISH)
            2 -> SimpleDateFormat("dd MM yyyy HH:mm:ss", Locale.ENGLISH)
            3 -> SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH)
            4 -> SimpleDateFormat("MM", Locale.ENGLISH)
            5 -> SimpleDateFormat("MMMM", Locale.getDefault())
            6 -> SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
            7 -> SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH)
            8 -> SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault())
            else -> SimpleDateFormat("dd MM yyyy HH:mm:ss", Locale.ENGLISH)
        }
        return parser.parse(dateTxt)!!
    }

    fun convertDateToString(type: Int, date: Date): String {
        val df = when (type) {
            1 -> SimpleDateFormat("E MMM dd HH:mm:ss z yyyy", Locale.ENGLISH)
            2 -> SimpleDateFormat("dd MM yyyy HH:mm:ss", Locale.ENGLISH)
            3 -> SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH)
            4 -> SimpleDateFormat("MM", Locale.ENGLISH)
            5 -> SimpleDateFormat("MMMM", Locale.getDefault())
            6 -> SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
            7 -> SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH)
            8 -> SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH)
            9 -> SimpleDateFormat("HH:mm", Locale.ENGLISH)
            10 -> SimpleDateFormat("d/M/yyyy", Locale.ENGLISH)
            11 -> SimpleDateFormat("d M yyyy", Locale.ENGLISH)
            else -> SimpleDateFormat("dd MM yyyy HH:mm:ss", Locale.ENGLISH)
        }
        return df.format(date)
    }

    fun convertStringToDateWithTimezone(type: Int, dateTxt: String): Date? {
        val parser = when (type) {
            1 -> SimpleDateFormat(
                "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'",
                Locale.getDefault()
            )
            else -> SimpleDateFormat(
                "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'",
                Locale.getDefault()
            )
        }
        parser.timeZone = TimeZone.getTimeZone("GMT")
        return parser.parse(dateTxt)
    }

    fun fromHtml(html: String?): Spanned {
        return when {
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.N -> Html.fromHtml(
                html!!,
                Html.FROM_HTML_MODE_LEGACY
            )
            else -> Html.fromHtml(html)
        }
    }

    fun showInfoDialog(
        context: Context
    ): AlertDialog {
        val dialog: AlertDialog
        val builder = AlertDialog.Builder(context).apply {
            setView(
                LayoutInflater.from(context).inflate(
                    R.layout.dialog_info,
                    null
                )
            )
        }
        dialog = builder.create()
//        val lp = WindowManager.LayoutParams()
//        lp.copyFrom(dialog.window?.attributes)
//        lp.width = WindowManager.LayoutParams.WRAP_CONTENT
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//        dialog.window?.attributes = lp
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)
        return dialog
    }

    fun showReportDialog(
        context: Context,
        clickCallback: ((AlertDialog) -> Unit)?
    ): AlertDialog {
        val dialog: AlertDialog
        val builder = AlertDialog.Builder(context).apply {
            setView(
                LayoutInflater.from(context).inflate(
                    R.layout.dialog_report,
                    null
                )
            )
        }
        dialog = builder.create()

//        val lp = WindowManager.LayoutParams()
//        lp.copyFrom(dialog.window?.attributes)
//        lp.width = WindowManager.LayoutParams.WRAP_CONTENT
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//        dialog.window?.attributes = lp

        dialog.show()

        dialog.layout_wrong_info.setOnClickListener {
            dialog.layout_wrong_info.tag = "1"
            dialog.layout_no_fish.tag = "0"
            dialog.layout_other.tag = "0"
            dialog.ic_check_1.visibility = View.VISIBLE
            dialog.ic_check_2.visibility = View.INVISIBLE
            dialog.ic_check_3.visibility = View.INVISIBLE
        }

        dialog.layout_no_fish.setOnClickListener {
            dialog.layout_wrong_info.tag = "0"
            dialog.layout_no_fish.tag = "1"
            dialog.layout_other.tag = "0"
            dialog.ic_check_1.visibility = View.INVISIBLE
            dialog.ic_check_2.visibility = View.VISIBLE
            dialog.ic_check_3.visibility = View.INVISIBLE
        }

        dialog.layout_other.setOnClickListener {
            dialog.layout_wrong_info.tag = "0"
            dialog.layout_no_fish.tag = "0"
            dialog.layout_other.tag = "1"
            dialog.ic_check_1.visibility = View.INVISIBLE
            dialog.ic_check_2.visibility = View.INVISIBLE
            dialog.ic_check_3.visibility = View.VISIBLE
        }

        dialog.tv_report.setOnClickListener {
            clickCallback?.invoke(dialog)
        }

        dialog.tv_cancel.setOnClickListener {
            dialog.dismiss()
        }

        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)
        return dialog
    }

    fun showDialog(context: Context, title: String, message: String, type: String) {
        val dialog: AlertDialog
        val builder = AlertDialog.Builder(context)
        builder.setTitle(title)
        builder.setMessage(message)
        builder.setPositiveButton("OK") { dialogInterface, _ ->
            dialogInterface.dismiss()
        }
        dialog = builder.create()
        dialog.show()
        dialog.setCanceledOnTouchOutside(false)
    }

    fun showCustomDialog(
        context: Context,
        type: Boolean,
        message: String,
        clickCallback: ((AlertDialog) -> Unit)?
    ): AlertDialog {
        val dialog: AlertDialog
        val builder = AlertDialog.Builder(context).apply {
            setView(
                LayoutInflater.from(context).inflate(
                    if (type) R.layout.dialog_custom_check else R.layout.dialog_custom_cross,
                    null
                )
            )
        }
        dialog = builder.create()
        dialog.show()
        dialog.tv_text.text = message
        dialog.tv_clickOk.setOnClickListener {
            clickCallback?.invoke(dialog)
        }
        dialog.setCanceledOnTouchOutside(false)
        return dialog
    }

    fun showCustomPromptDialog(
        context: Context,
        message: String,
        acceptCallback: ((AlertDialog) -> Unit)?,
        rejectCallback: ((AlertDialog) -> Unit)?
    ): AlertDialog {
        val dialog: AlertDialog
        val builder = AlertDialog.Builder(context).apply {
            setView(
                LayoutInflater.from(context).inflate(
                    R.layout.dialog_custom_prompt,
                    null
                )
            )
        }
        dialog = builder.create()
        dialog.show()
        dialog.tv_text.text = message
        dialog.tv_click_accept.setOnClickListener {
            acceptCallback?.invoke(dialog)
        }
        dialog.tv_click_reject.setOnClickListener {
            rejectCallback?.invoke(dialog)
        }
        dialog.setCanceledOnTouchOutside(false)
        return dialog
    }

    fun hideKeyboard(activity: Activity) {
        val imm =
            activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view: View? = activity.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun dialogLoading(activity: Activity): Dialog {
        val dialog = Dialog(activity)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.dialog_loading)
        return dialog
    }

    fun bitmapToFile(context: Context, bitmap: Bitmap): Uri {
        // Get the context wrapper
        val wrapper = ContextWrapper(context)

        // Initialize a new file instance to save bitmap object
        var file = wrapper.getDir("Images", Context.MODE_PRIVATE)
        file = File(file, "${System.currentTimeMillis()}.jpg")

        try {
            // Compress the bitmap and save in jpg format
            val stream: OutputStream = FileOutputStream(file)
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream)
            stream.flush()
            stream.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }

        // Return the saved bitmap uri
        return Uri.parse(file.absolutePath)
    }

    fun toastShort(context: Context, message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    fun showFullImage(context: Context, imgUrl: String) {
        val dialog = Dialog(context, android.R.style.Theme_Black_NoTitleBar_Fullscreen)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.show_full_image)

        val webView = dialog.findViewById(R.id.web_view) as WebView
        webView.loadUrl(imgUrl)

        webView.settings.loadWithOverviewMode = true;
        webView.settings.useWideViewPort = true;

        webView.settings.builtInZoomControls = true
        webView.settings.setSupportZoom(true)

        dialog.show()
    }

    fun createFormDataImage(name: String, imageFile: File): MultipartBody.Part {
        return MultipartBody.Part.createFormData(
            name,
            imageFile.name,
            RequestBody.create("image/*".toMediaTypeOrNull(), imageFile)
        )
    }

    fun createFormDataString(name: String, text: String): MultipartBody.Part {
        return MultipartBody.Part.createFormData(
            name,
            text
        )
    }

    fun createPartFromString(descriptionString: String): RequestBody? {
        return RequestBody.create(
            MultipartBody.FORM, descriptionString
        )
    }

    fun getDifferenceTime(time: Long): String {
        var different = Calendar.getInstance().timeInMillis - time
        val secondsInMilli = 1000
        val minutesInMilli = secondsInMilli * 60
        val hoursInMilli = minutesInMilli * 60
        val daysInMilli = hoursInMilli * 60

        val elapsedDays = different / daysInMilli
        different %= daysInMilli

        val elapsedHours = different / hoursInMilli
        different %= hoursInMilli

        val elapsedMinutes = different / minutesInMilli
        different %= minutesInMilli

        val elapsedSeconds = different / secondsInMilli

        return when {
            elapsedDays >= 1L -> elapsedDays.toString() + "d"
            elapsedHours >= 1L -> elapsedHours.toString() + "h"
            elapsedMinutes >= 1L -> elapsedMinutes.toString() + "m"
            else -> elapsedSeconds.toString() + "s"
        }

    }

    fun getHomeDifferenceTime(time: Long): String {
        var different = Calendar.getInstance().timeInMillis - time
        val secondsInMilli = 1000
        val minutesInMilli = secondsInMilli * 60
        val hoursInMilli = minutesInMilli * 60
        val daysInMilli = hoursInMilli * 24

        val elapsedDays = different / daysInMilli
        different %= daysInMilli

        val elapsedHours = different / hoursInMilli
        different %= hoursInMilli

        val elapsedMinutes = different / minutesInMilli
        different %= minutesInMilli

        val elapsedSeconds = different / secondsInMilli

        return when {
            elapsedDays >= 1L -> elapsedDays.toString() + if (elapsedDays.toString().toInt() <= 1) " day ago" else " days ago"
            elapsedHours >= 1L -> elapsedHours.toString() + if (elapsedHours.toString().toInt() <= 1) " hour ago" else " hours ago"
            elapsedMinutes >= 1L -> elapsedMinutes.toString() + if (elapsedMinutes.toString().toInt() <= 1) " minute ago" else " minutes ago"
            else -> if (elapsedSeconds.toString().toInt() <= 0) "0" else elapsedSeconds.toString() + if (elapsedSeconds.toString().toInt() <= 1) " second ago" else " seconds ago"
        }

    }

}