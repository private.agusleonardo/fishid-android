package com.ta.fishid.util.zoomEvent

import android.view.ScaleGestureDetector
import android.widget.ImageView

class ScaleListener (val img : ImageView) : ScaleGestureDetector.SimpleOnScaleGestureListener() {
    private var mScaleFactor = 1.0f

    override fun onScale(scaleGestureDetector: ScaleGestureDetector): Boolean {
        mScaleFactor *= scaleGestureDetector.scaleFactor
        mScaleFactor = Math.max(
            0.1f,
            Math.min(mScaleFactor, 10.0f)
        )
        img.setScaleX(mScaleFactor)
        img.setScaleY(mScaleFactor)
        return true
    }
}