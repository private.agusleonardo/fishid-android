package com.ta.fishid.util

interface OnLoadMoreListener {
    fun onLoadMore()
}