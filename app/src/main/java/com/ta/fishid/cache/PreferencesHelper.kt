package com.ta.fishid.cache

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import com.ta.fishid.model.UserData
import com.ta.fishid.ui.login.LoginActivity
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PreferencesHelper @Inject constructor(private val context: Context) {
    companion object {
        private const val keyPackageName = "fishid"
        private const val keyJwtToken = "jwtToken"
        private const val keyUserId = "userId"
        private const val keyUserName = "userName"
        private const val keyBio = "bio"
        private const val keyRememberMe = "rememberMe"
        private const val keyProfilePicture = "profilePicture"
        private const val keyFirebaseToken = "firebaseToken"
    }

    private val atPref: SharedPreferences =
        context.getSharedPreferences(keyPackageName, Context.MODE_PRIVATE)

    var rememberMe: Boolean
        get() = atPref.getBoolean(keyRememberMe, false)
        set(rememberMe) = atPref.edit().putBoolean(keyRememberMe, rememberMe).apply()

    fun setJwtToken(token: String) {
        atPref.edit().putString(keyJwtToken, token).apply()
    }

    fun getJwtToken(): String {
        return atPref.getString(keyJwtToken, "")!!
    }

    fun saveAccount(userData: UserData) {
        atPref.edit().putBoolean(keyRememberMe, true).apply()
        atPref.edit().putString(keyUserId, userData.userId ?: "").apply()
        atPref.edit().putString(keyUserName, userData.username ?: "").apply()
        atPref.edit().putString(keyBio, userData.bio ?: "").apply()
        atPref.edit().putString(keyProfilePicture, userData.profilePict ?: "").apply()
    }

    fun setFcmToken(token: String) {
        atPref.edit().putString(keyFirebaseToken, token).apply()
    }

    fun getFcmToken(): String {
        return atPref.getString(keyFirebaseToken, "")!!
    }

    fun getAccountRx(): Single<UserData> {
        val user = UserData(
            userId = atPref.getString(keyUserId, null),
            profilePict = atPref.getString(keyProfilePicture, null),
            username = atPref.getString(keyUserName, null),
            bio = atPref.getString(keyBio, null)
        )
        return Single.just(user)
    }

    fun clearSession() {
//        val fcm = getUserToken()
        atPref.edit().clear().apply()
//        saveUserToken(fcm)
        val i = Intent(context, LoginActivity::class.java)
        i.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        context.startActivity(i)
    }

}