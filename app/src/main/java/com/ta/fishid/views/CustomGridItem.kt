package com.ta.fishid.views

import android.content.Context
import android.util.AttributeSet
import android.widget.RelativeLayout


class GridItem : RelativeLayout {
    constructor(context: Context?) : super(context) {}
    constructor(context: Context?, attr: AttributeSet?) : super(context, attr) {}
    constructor(context: Context?, attr: AttributeSet?, integer: Int) : super(
        context,
        attr,
        integer
    ) {
    }

    // Override onMeasure to give the view the same height as the specified width
    public override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec)
        setMeasuredDimension(measuredWidth, measuredWidth)
    }
}