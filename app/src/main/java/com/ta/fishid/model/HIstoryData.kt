package com.ta.fishid.model

import com.google.gson.annotations.SerializedName


data class HistoryData(
    @SerializedName("fish")
    var fish: List<Fish>,
    @SerializedName("scan_id")
    var scanId: Int,
    @SerializedName("waktu")
    var waktu: String,
    var expanded: Boolean = false
)

data class Fish(
    @SerializedName("jumlah")
    var jumlah: Int,
    @SerializedName("nama_latin")
    var namaLatin: String
)