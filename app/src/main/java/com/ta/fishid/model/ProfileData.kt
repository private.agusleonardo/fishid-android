package com.ta.fishid.model

import com.google.gson.annotations.SerializedName

data class ProfileData(
    @SerializedName("user")
    val user: UserData,
    @SerializedName("post")
    val post: List<PostData>
)