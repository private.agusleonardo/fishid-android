package com.ta.fishid.model

import com.google.gson.annotations.SerializedName

data class CommentData(
    @SerializedName("user_id")
    var userId: Int,
    @SerializedName("comment_status")
    var commentStatus: Boolean,
    @SerializedName("comments")
    var comments: List<Comments> = arrayListOf()
)

data class Comments(
    @SerializedName("user_id")
    var userId: Int,
    @SerializedName("username")
    var username: String,
    @SerializedName("profile_pict")
    var profilePict: String,
    @SerializedName("comment")
    var comment: String,
    @SerializedName("waktu")
    var waktu: String
)