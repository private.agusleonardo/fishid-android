package com.ta.fishid.model
import com.google.gson.annotations.SerializedName

data class DetectData(
    @SerializedName("fish_id")
    var fishId: String,
    @SerializedName("nama_latin")
    var namaLatin: String,
    @SerializedName("nama_inggris")
    var namaInggris: String,
    @SerializedName("deskripsi")
    var deskripsi: String,
    @SerializedName("fish_picture")
    var fishPicture: String,
    @SerializedName("bentuk_tubuh")
    var bentukTubuh: String,
    @SerializedName("bentuk_mulut")
    var bentukMulut: String,
    @SerializedName("bentuk_sirip")
    var bentukSirip: String,
    @SerializedName("bentuk_ekor")
    var bentukEkor: String,
    @SerializedName("corak_tubuh")
    var corakTubuh: String,
    @SerializedName("probability")
    var probability: Double,
    @SerializedName("species")
    var species: String,
    @SerializedName("x1")
    var x1: Int,
    @SerializedName("x2")
    var x2: Int,
    @SerializedName("y1")
    var y1: Int,
    @SerializedName("y2")
    var y2: Int,
    @SerializedName("status")
    var status: Boolean
)