package com.ta.fishid.model

import com.google.gson.annotations.SerializedName

data class UserData(
    @SerializedName("user_id")
    var userId: String? = "",
    @SerializedName("profile_pict")
    var profilePict: String? = "",
    @SerializedName("username")
    var username: String? = "",
    @SerializedName("bio")
    var bio: String? = ""
)