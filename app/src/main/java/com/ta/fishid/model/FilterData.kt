package com.ta.fishid.model

import com.google.gson.annotations.SerializedName

data class FilterData(
    @SerializedName("fish_id")
    var fishId: String,
    @SerializedName("nama_latin")
    var namaLatin: String,
    @SerializedName("nama_inggris")
    var namaInggris: String,
    @SerializedName("fish_picture")
    var fishPicture: String
)