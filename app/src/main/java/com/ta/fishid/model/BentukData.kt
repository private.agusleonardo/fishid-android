package com.ta.fishid.model

data class BentukData(
    val picture: Int,
    val name: String
)