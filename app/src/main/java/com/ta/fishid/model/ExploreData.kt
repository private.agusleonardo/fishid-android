package com.ta.fishid.model

import com.google.gson.annotations.SerializedName

data class ExploreData(
    @SerializedName("fish")
    var fish: DetectData,
    @SerializedName("post")
    var post: List<PostData>
)