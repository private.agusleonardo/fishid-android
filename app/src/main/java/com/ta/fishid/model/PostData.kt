package com.ta.fishid.model

import com.google.gson.annotations.SerializedName

data class PostData(
    @SerializedName("bentuk_ekor")
    var bentukEkor: String,
    @SerializedName("bentuk_mulut")
    var bentukMulut: String,
    @SerializedName("bentuk_sirip")
    var bentukSirip: String,
    @SerializedName("bentuk_tubuh")
    var bentukTubuh: String,
    @SerializedName("caption")
    var caption: String,
    @SerializedName("corak_tubuh")
    var corakTubuh: String,
    @SerializedName("image")
    var image: String,
    @SerializedName("post_id")
    var postId: Int,
    @SerializedName("profile_pict")
    var profilePict: String,
    @SerializedName("user_id")
    var userId: Int,
    @SerializedName("username")
    var username: String,
    @SerializedName("like_status")
    var likeStatus: Boolean,
    @SerializedName("like_count")
    var likeCount: Int,
    @SerializedName("comment_count")
    var commentCount: Int,
    @SerializedName("probability")
    var probability: Double,
    @SerializedName("species")
    var species: String?,
    @SerializedName("x1")
    var x1: Int,
    @SerializedName("x2")
    var x2: Int,
    @SerializedName("y1")
    var y1: Int,
    @SerializedName("y2")
    var y2: Int,
    @SerializedName("tanggal")
    var tanggal: String,
    @SerializedName("comment_status")
    var commentStatus: Boolean
)